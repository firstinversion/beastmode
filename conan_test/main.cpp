#include <beastmode/endpoint.hpp>
#include <beastmode/server.hpp>
#include <beastmode/service.hpp>

class echo : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get() { response().body() = "echo"; }

    void post() { response().body() = request().body(); }

    void put() { response().body() = request().body(); }
};

int main() {
    auto server = beastmode::server{};
    server.add_endpoint<echo>("/echo");
    server.start(8080);

    auto exe_thread = std::thread{[&]() { server.join(); }};
    std::this_thread::sleep_for(std::chrono::seconds(1));
    server.stop();
    exe_thread.join();

    return 0;
}
