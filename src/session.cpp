#include <beastmode/session.hpp>

#include <spdlog/spdlog.h>

namespace beastmode {

    session::session(router& router, ip::tcp::socket&& socket)
        : _router(router)
        , _socket(std::move(socket)) {}

    void session::start(asio::yield_context yield) {
        try {
            run(yield);
        } catch (const std::exception& err) {
            spdlog::error("Exception {}: {}", err.what(), _request.base().target());
        }
    }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    void session::run(asio::yield_context yield) {
        bool close = false;
        while (!close) {
            _request  = {};
            _response = {};

            auto ec                = boost::system::error_code{};
            auto bytes_transferred = http::async_read(_socket, _buffer, _request, yield[ec]);
            boost::ignore_unused(bytes_transferred);

            if (ec == http::error::end_of_stream) break;
            if (ec) throw std::system_error(ec);

            _response = {http::status::ok, _request.version()};

            _response.set(http::field::server, BOOST_BEAST_VERSION_STRING);
            _response.set(http::field::content_type, "text/plain");
            _response.keep_alive(_request.keep_alive());

            _router.bind_resource(yield);  // TODO: Is this implementation thread unsafe?
            _router.bind_resource(_request);
            _router.bind_resource(_response);

            _router.handle(route{std::string{_request.base().target()}, _request.base().method()}, _request, _response);
            _router.unbind_resource(_request);
            _router.unbind_resource(_response);
            _router.unbind_resource(
                yield);  // TODO: Resource unbinding is probably a bad idea generally. Once the setup phase has
                         // completed, users should not be tampering with available resources.
            _response.prepare_payload();

            close = _response.need_eof();

            bytes_transferred = http::async_write(_socket, _response, yield[ec]);
            boost::ignore_unused(bytes_transferred);
            if (ec) throw std::system_error(ec);
        }

        _socket.shutdown(ip::tcp::socket::shutdown_send);
    }
#pragma clang diagnostic pop

}  // namespace beastmode
