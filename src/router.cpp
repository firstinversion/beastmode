#include <iostream>  // Why does iostream have to go first?

#include "beastmode/router.hpp"
#include "beastmode/query_parameter.hpp"
#include <boost/beast/http/status.hpp>

namespace beastmode {

    router::router() {}

    void router::handle(const route& route, const http::request<http::string_body>& req,
                        http::response<http::string_body>& res) {
        auto result = _route_map.process_url_target(route.path);
        if (result.data != nullptr) {
            endpoint_func& f = (*result.data)[route.verb];
            if (f) {
                // TODO: Copying is a crappy way to do this
                auto resource_map_copy = _resource_map;
                post_path post_path_val;
                post_path_val.sv = result.post_path;
                resource_map_copy.try_emplace(std::type_index(typeid(post_path)), &post_path_val);
                f(result.captures, resource_map_copy, req, res);
            } else {
                res.base().result(http::status::not_found);
                res.body() = "The resource '" + route.path + "' does not support the " +
                             std::string(http::to_string(route.verb)) + "method.";
            }
        } else {
            res.base().result(http::status::not_found);
            res.body() = "The resource '" + route.path + "' was not found.";
        }
    }

}  // namespace beastmode
