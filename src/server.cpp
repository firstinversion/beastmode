#include <beastmode/server.hpp>

#include <beastmode/session.hpp>
#include <spdlog/spdlog.h>

namespace beastmode {

    void server::start(uint16_t port) { start({ip::tcp::v4(), port}); }

    void server::start(ip::tcp::endpoint endpoint) {
        if (_executor) throw std::runtime_error("Executor already exists cannot start beastmode server.");
        _executor.emplace(std::move(endpoint), _router);
    }

    void server::join() {
        if (_executor) _executor->_ioc.run();
    }

    void server::stop() {
        if (_executor) _executor->_ioc.stop();
    }

    server::executor::executor(ip::tcp::endpoint endpoint, router& router)
        : _ioc()
        , _endpoint(std::move(endpoint))
        , _acceptor(_ioc)
        , _router(router) {
        boost::system::error_code ec;

        _acceptor.open(endpoint.protocol(), ec);
        if (ec) throw std::system_error(ec);

        _acceptor.set_option(asio::socket_base::reuse_address(true), ec);
        if (ec) throw std::system_error(ec);

        _acceptor.bind(_endpoint, ec);
        if (ec) throw std::system_error(ec);

        _acceptor.listen(asio::socket_base::max_listen_connections, ec);
        if (ec) throw std::system_error(ec);

        do_accept();
    }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "InfiniteRecursion"
    void server::executor::do_accept() {
        _acceptor.async_accept(_ioc, [&](boost::system::error_code ec, ip::tcp::socket socket) {
            if (ec) {
                spdlog::error("Failed on do_accept ({})", ec.message());
            } else {
                asio::spawn(_ioc, [& router = _router, socket = std::move(socket)](asio::yield_context yield) mutable {
                    auto session = beastmode::session{router, std::move(socket)};
                    session.start(yield);
                    // TODO: this is where the joint for using alternate co-routines could go.
                });
            }

            do_accept();
        });
    }
#pragma clang diagnostic pop

}  // namespace beastmode
