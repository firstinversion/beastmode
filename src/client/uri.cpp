#include <beastmode/client/uri.hpp>

#include <beastmode/client/internal/uri_grammar.hpp>
#include <fmt/format.h>

namespace fmt {

    template <>
    struct formatter<beastly::protocol> : formatter<string_view> {
        template <typename FormatContext>
        auto format(beastly::protocol p, FormatContext& ctx) {
            string_view name = "unknown";
            switch (p) {
            case beastly::protocol::http: name = "http"; break;
            case beastly::protocol::https: name = "https"; break;
            }
            return formatter<string_view>::format(name, ctx);
        }
    };

}  // namespace fmt

namespace beastly {

    uri::uri(const std::string& uri) {
        static const internal::uri_skipper<std::string::const_iterator> skipper;
        static const internal::uri_grammar<std::string::const_iterator> grammar;
        bool r = boost::spirit::qi::phrase_parse(uri.begin(), uri.end(), grammar, skipper, *this);
        if (!r) throw std::invalid_argument("uri was malformed");
        if (_target.empty()) _target = "/";
    }

    uri::uri(const char* uri) {
        static const internal::uri_skipper<const char*> skipper;
        static const internal::uri_grammar<const char*> grammar;

        auto begin = uri;
        auto end   = strlen(uri) + begin;
        bool r     = boost::spirit::qi::phrase_parse(begin, end, grammar, skipper, *this);
        if (!r) throw std::invalid_argument("uri was malformed");
        if (_target.empty()) _target = "/";
    }

    uri::uri(std::string_view uri) {
        static const internal::uri_skipper<std::string_view::const_iterator> skipper;
        static const internal::uri_grammar<std::string_view::const_iterator> grammar;
        bool r = boost::spirit::qi::phrase_parse(uri.begin(), uri.end(), grammar, skipper, *this);
        if (!r) throw std::invalid_argument("uri was malformed");
        if (_target.empty()) _target = "/";
    }

    std::string uri::str() const {
        if (_host.empty()) {
            return fmt::format("{}://{}", _protocol, _target);
        } else if (_port.empty()) {
            return fmt::format("{}://{}{}", _protocol, _host, _target);
        } else {
            return fmt::format("{}://{}:{}{}", _protocol, _host, _port, _target);
        }
    }

}  // namespace beastly
