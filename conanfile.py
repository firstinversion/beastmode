from conans import ConanFile, CMake, tools


class BeastMode(ConanFile):
    name = "beastmode"
    version = "0.5.0"
    license = "MIT"
    author = ("Luke Allen <nightwolf55la@gmail.com>", "Andrew Rademacher <andrewrademacher@icloud.com>")
    url = "https://bitbucket.org/firstinversion/beastmode/admin"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "build_example": [True, False]}
    default_options = "shared=False", "build_example=False"
    generators = "cmake"
    exports_sources = "*", "!test_server/*", "!.idea/*", "!conan_test/*", "!build/*", "!cmake-build*/*", "!tools/*"
    requires = ("boost/1.71.0@conan/stable",
                "spdlog/1.4.2@bincrafters/stable",
                "zlib/1.2.11@conan/stable",
                "OpenSSL/1.0.2p@conan/stable")
    build_requires = ("Catch2/2.10.2@catchorg/stable",
                      "jsonformoderncpp/3.7.0@vthiery/stable",
                      "rapidmessage/0.5.11@firstinversion/testing")

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={'USE_CONAN': True, 'BUILD_STATIC': not self.options.shared, 'BUILD_EXAMPLE': self.options.build_example})
        cmake.build()
        if tools.get_env("CONAN_RUN_TEST", False):
            cmake.test(output_on_failure=True)

    def package(self):
        cmake = CMake(self)
        cmake.install()

    # def package(self):
    #     self.copy("**/*", dst="include", src="include")
    #     self.copy("*.lib", dst="lib", keep_path=False)
    #     self.copy("*.dll", dst="bin", keep_path=False)
    #     self.copy("*.dylib*", dst="lib", keep_path=False)
    #     self.copy("*.so", dst="lib", keep_path=False)
    #     self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["beastmode"]
