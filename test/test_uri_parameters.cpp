#include <catch2/catch.hpp>

#include <beastmode/client/beastly.hpp>
#include <beastmode/endpoint.hpp>
#include <beastmode/server.hpp>
#include <rapidmessage/object.hpp>
#include <rapidmessage/schema.hpp>

#include "./with_server.hpp"

struct user_struct {
    int32_t     id;
    std::string first_name;
    std::string last_name;
};

RAPID_OBJECT(user_object) {
public:
    RAPID_OBJECT_CONSTRUCTORS(user_object);
    RAPID_PROP_BYVAL(int32_t, id);
    RAPID_PROP_STRING(first_name);
    RAPID_PROP_STRING(last_name);

    const rapidjson::SchemaDocument& schema() const {
        namespace s              = rapidmessage::schema;
        static const auto schema = s::compile(s::object(s::property("id", s::integer{}),
                                                        s::property("first_name", s::string{}),
                                                        s::property("last_name", s::string{})));
        return schema;
    }
};

RAPID_MESSAGE(single_user) {
public:
    RAPID_MESSAGE_CONSTRUCTORS(single_user);
    RAPID_PROP_BYREF(user_object, data);

    const rapidjson::SchemaDocument& schema() const override {
        namespace s              = rapidmessage::schema;
        static const auto schema = s::compile(s::object(s::property("data",
                                                                    s::object(s::property("id", s::integer{}),
                                                                              s::property("first_name", s::string{}),
                                                                              s::property("last_name", s::string{})))));
        return schema;
    }
};

RAPID_MESSAGE(multiple_users) {
public:
    RAPID_MESSAGE_CONSTRUCTORS(multiple_users);
    RAPID_PROP_BYREF(rapidmessage::array<user_object>, data);

    const rapidjson::SchemaDocument& schema() const override {
        namespace s = rapidmessage::schema;
        static const auto schema =
            s::compile(s::object(s::property("data",
                                             s::array(s::items(s::object(s::property("id", s::integer{}),
                                                                         s::property("first_name", s::string{}),
                                                                         s::property("last_name", s::string{})))))));
        return schema;
    }
};

class users_endpoint : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get() {
        multiple_users users;
        std::for_each(users_db.begin(), users_db.end(), [&](user_struct& user) {
            auto d = users.data().insert();
            d.id(user.id);
            d.first_name(user.first_name);
            d.last_name(user.last_name);
        });
        response().body() = users.to_json().GetString();
    }

private:
    static inline std::vector<user_struct> users_db = {{0, "John", "Doe"}, {1, "Jane", "Doe"}, {2, "Steve", "O"}};
};

void setup_uri_parameters(beastmode::server& server) { server.add_endpoint<users_endpoint>("/users"); }

TEST_CASE("parameter free route", "[uri_parameters]") {
    with_server(&setup_uri_parameters, []() {
        auto        r = beastly::get("http://localhost:8080/users");
        std::string b = r.body();
        REQUIRE(r.result() == beastmode::http::status::ok);
        multiple_users us(r.body().data());
        REQUIRE(us.data()[0].id() == 0);
        REQUIRE(us.data()[0].first_name() == "John");
        REQUIRE(us.data()[1].id() == 1);
        REQUIRE(us.data()[1].first_name() == "Jane");
    })();
}

// TEST_CASE("capture parameter route 1", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get("http://localhost:8080/users/0");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    single_user u(r.body().data());
//    REQUIRE(u.data().id() == 0);
//    REQUIRE(u.data().first_name() == "John");
//  })();
//}

// TEST_CASE("capture parameter route 2", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get("http://localhost:8080/users/1");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    single_user u(r.body().data());
//    REQUIRE(u.data().id() == 1);
//    REQUIRE(u.data().first_name() == "Jane");
//  })();
//}

TEST_CASE("capture parameter missing", "[uri_parameters]") {
    with_server(&setup_uri_parameters, []() {
        auto r = beastly::get("http://localhost:8080/users/100");
        REQUIRE(r.result() == beastmode::http::status::not_found);
    })();
}

TEST_CASE("query parameter route 1", "[uri_parameters]") {
    with_server(&setup_uri_parameters, []() {
        auto r = beastly::get("http://localhost:8080/users?first_name=John");
        REQUIRE(r.result() == beastmode::http::status::ok);
        multiple_users us(r.body().data());
        REQUIRE(us.data()[0].id() == 0);
        REQUIRE(us.data()[0].first_name() == "John");
    })();
}

// TEST_CASE("query parameter route 2", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get("http://localhost:8080/users?first_name=Jane");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    multiple_users us(r.body().data());
//    REQUIRE(us.data()[0].id() == 0);
//    REQUIRE(us.data()[0].first_name() == "Jane");
//  })();
//}

// TEST_CASE("query parameter missing", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get("http://localhost:8080/users?first_name=Missing");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    multiple_users us(r.body().data());
//    REQUIRE(us.data().empty());
//  })();
//}

// TEST_CASE("query param with multiple results", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get("http://localhost:8080/users?last_name=Doe");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    multiple_users us(r.body().data());
//    REQUIRE(us.data().size() == 2);
//    REQUIRE(us.data()[0].first_name() == "John");
//    REQUIRE(us.data()[1].first_name() == "Jane");
//  })();
//}

// TEST_CASE("query param with multiples of the same field", "[uri_parameters]") {
//  boost::asio::io_context ioc;
//  with_server(ioc, &setup_uri_parameters, []() {
//    auto r = beastly::get(
//        "http://localhost:8080/users?first_name=Jane&first_name=Steve");
//    REQUIRE(r.result() == beastmode::http::status::ok);
//    multiple_users us(r.body().data());
//    REQUIRE(us.data().size() == 2);
//    REQUIRE(us.data()[0].first_name() == "Jane");
//    REQUIRE(us.data()[1].first_name() == "Steve");
//  })();
//}

TEST_CASE("non-existant route", "[uri_parameters]") {
    with_server(&setup_uri_parameters, []() {
        auto r = beastly::get("http://localhost:8080/does-not-exist");
        REQUIRE(r.result() == beastmode::http::status::not_found);
    })();
}
