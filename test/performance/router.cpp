#include <catch2/catch.hpp>

#include <beastmode/endpoint.hpp>
#include <beastmode/server.hpp>
#include <beastmode/service.hpp>

#if defined(CATCH_CONFIG_ENABLE_BENCHMARKING)

namespace {

    struct resource : beastmode::resource_base {
        int i = 0;
    };

    struct endpoint1 : beastmode::endpoint {
        using endpoint::endpoint;
        void post(resource& r) { r.i -= 3; }
    };

    struct endpoint2 : beastmode::endpoint {
        using endpoint::endpoint;
        void post(resource& r) { r.i += 5; }
    };

    struct endpoint3 : beastmode::endpoint {
        using endpoint::endpoint;
        void get(resource& r) { r.i += 7; }
    };

    struct endpoint4 : beastmode::endpoint {
        using endpoint::endpoint;
        void put(resource& r) { r.i -= 1; }
    };
}  // namespace

TEST_CASE("Benchmark Routing", "[benchmark]") {
    SECTION("Small number of routes") {
        resource r;

        beastmode::router router;
        router.bind_resource(r);

        router.add_route<&endpoint1::post>(beastmode::route{"/my_test/testing/1", beastmode::http::verb::post});
        router.add_route<&endpoint2::post>(beastmode::route{"/my_test/testing/2", beastmode::http::verb::post});
        router.add_route<&endpoint3::get>(beastmode::route{"/my_test/testing/3", beastmode::http::verb::get});
        router.add_route<&endpoint4::put>(beastmode::route{"/my_test/testing/4", beastmode::http::verb::put});

        beastmode::http::request<beastmode::http::string_body>  request;
        beastmode::http::response<beastmode::http::string_body> response;

        BENCHMARK("new way") {
            for (int i = 0; i < 1'000'000; i++) {
                if (i % 5 == 0 && i % 7 == 0) {
                    router.handle_new(
                        beastmode::route{"/my_test/testing/1", beastmode::http::verb::post}, request, response);
                } else if (i % 5 == 0) {
                    router.handle_new(
                        beastmode::route{"/my_test/testing/2", beastmode::http::verb::post}, request, response);
                } else if (i % 7 == 0) {
                    router.handle_new(
                        beastmode::route{"/my_test/testing/3", beastmode::http::verb::get}, request, response);
                } else {
                    router.handle_new(
                        beastmode::route{"/my_test/testing/4", beastmode::http::verb::put}, request, response);
                }
            }
        }

        BENCHMARK("old way") {
            for (int i = 0; i < 1'000'000; i++) {
                if (i % 5 == 0 && i % 7 == 0) {
                    router.handle(
                        beastmode::route{"/my_test/testing/1", beastmode::http::verb::post}, request, response);
                } else if (i % 5 == 0) {
                    router.handle(
                        beastmode::route{"/my_test/testing/2", beastmode::http::verb::post}, request, response);
                } else if (i % 7 == 0) {
                    router.handle(
                        beastmode::route{"/my_test/testing/3", beastmode::http::verb::get}, request, response);
                } else {
                    router.handle(
                        beastmode::route{"/my_test/testing/4", beastmode::http::verb::put}, request, response);
                }
            }
        }
    }

    SECTION("Many routes") {
        std::vector<std::string> some_strings{
            "my_string", "something", "something_else", "lkdsznvbe", "qweurytosyc", "a"};
    }
}

#endif
