#include <catch2/catch.hpp>
#include <sstream>

#include <beastmode/route_map.hpp>
#include <unordered_map>

TEST_CASE("route_map - strings") {
    beastmode::route_map<int> rm;
    std::stringstream         stream{};

    SECTION("Empty") {
        rm.print(stream);
        CHECK(stream.str() == "");
    }

    auto insert_and_check_result = [&rm](const std::string& key, int value, std::optional<int> result_value = {}) {
        INFO("Inserting");
        INFO(key)
        auto result = rm.insert(beastmode::make_path(key), value);
        if (result_value) {
            CHECK(*result.first == *result_value);
            CHECK_FALSE(result.second);
        } else {
            CHECK(*result.first == value);
            CHECK(result.second);
        }
    };

    insert_and_check_result("/test/", 4);
    SECTION("Insert1") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test/\" = 4\n"
              "");
    }

    // Add something that matches a leaf node (/test/)
    insert_and_check_result("/test/test/", 1);
    SECTION("Insert2") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test/\" = 4\n"
              "  't' + \"est/\" = 1\n"
              "");
    }

    // Add something that partially matches a leaf node (/test/test)
    insert_and_check_result("/test/testing/", 2);
    SECTION("Insert3") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test/\" = 4\n"
              "  't' + \"est\" = nullopt\n"
              "    'i' + \"ng/\" = 2\n"
              "    '/' + \"\" = 1\n"
              "");
    }

    // Add something that partially matches just an edge (/)
    insert_and_check_result("/blah/foo/", 3);
    SECTION("Insert4") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/foo/\" = 3\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Nothing new tested here, setting up for next test
    insert_and_check_result("/blah/bar/", 5);
    SECTION("Insert5") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = nullopt\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo/\" = 3\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Adding something that matches an existing non-leaf node without data
    insert_and_check_result("/blah/", 6);
    SECTION("Insert6") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo/\" = 3\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Adding something that matches an existing non-leaf node with data already
    insert_and_check_result("/blah/", 7, 6);
    SECTION("Insert7") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo/\" = 3\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Add something that fully matches a non-leaf node (/blah/), edge exists (f) but it is not the first edge, forcing
    // linked list traversal
    insert_and_check_result("/blah/foobar/", 11);
    SECTION("Insert8") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Add something that fully matches a non-leaf node, but then no edge already exists for it forcing creation of new
    // next node (/blah/)
    insert_and_check_result("/blah/something/", 12);
    SECTION("Insert9") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "    's' + \"omething/\" = 12\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "");
    }

    // Prep for next test
    insert_and_check_result("/some/thing/", 9);
    SECTION("Insert10") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "    's' + \"omething/\" = 12\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "  's' + \"ome/thing/\" = 9\n"
              "");
    }

    // Adding something that matches the front of a longer compression string
    insert_and_check_result("/some/", 10);
    SECTION("Insert11") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "    's' + \"omething/\" = 12\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "  's' + \"ome/\" = 10\n"
              "    't' + \"hing/\" = 9\n"
              "");
    }

    // Adding something that requires percent encoding
    insert_and_check_result("/percent encoded/", 13);
    SECTION("Insert12") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "    's' + \"omething/\" = 12\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "  's' + \"ome/\" = 10\n"
              "    't' + \"hing/\" = 9\n"
              "  'p' + \"ercent encoded/\" = 13\n"
              "");
    }

    // Adding something that requires percent encoding at the end of the string
    insert_and_check_result("/another^percent encoded ", 14);
    SECTION("Insert13") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"\" = nullopt\n"
              "  'b' + \"lah/\" = 6\n"
              "    'b' + \"ar/\" = 5\n"
              "    'f' + \"oo\" = nullopt\n"
              "      'b' + \"ar/\" = 11\n"
              "      '/' + \"\" = 3\n"
              "    's' + \"omething/\" = 12\n"
              "  't' + \"est/\" = 4\n"
              "    't' + \"est\" = nullopt\n"
              "      'i' + \"ng/\" = 2\n"
              "      '/' + \"\" = 1\n"
              "  's' + \"ome/\" = 10\n"
              "    't' + \"hing/\" = 9\n"
              "  'p' + \"ercent encoded/\" = 13\n"
              "  'a' + \"nother^percent encoded /\" = 14\n"
              "");
    }

    SECTION("Find") {
        // Find tests
        std::unordered_map<std::string, int> find_tests{
            {"/test", 4},
            {"/test/test", 1},
            {"/test/testing", 2},
            {"/blah/foo", 3},
            {"/blah/bar", 5},
            {"/blah", 6},
            {"/blah/foobar", 11},
            {"/blah/something", 12},
            {"/some/thing", 9},
            {"/some", 10},
            {"/percent encoded", 13},
            {"/percent%20encoded", 13},
            {"/another^percent encoded ", 14},
            {"/another%5epercent encoded ", 14},
            {"/another%5epercent%20encoded ", 14},
            {"/another%5epercent encoded%20", 14},
            {"/another%5epercent%20encoded%20", 14},
            {"/another^percent%20encoded ", 14},
            {"/another^percent%20encoded%20", 14},
            {"/another^percent encoded%20", 14},
            {"/another%5Epercent encoded ", 14},
            {"/another%5Epercent%20encoded ", 14},
            {"/another%5Epercent encoded%20", 14},
            {"/another%5Epercent%20encoded%20", 14},
        };

        std::vector<std::string> not_find_tests = {
            "",
            "/testing",
            "/test/testi",
            "/test%20test",
            "/test%20test%20",
        };

        std::vector<std::string> appendages = {
            "",
            "/",
            "?",
            "/?",
            "#",
            "/#",
            "?#",
            "/?#",
            "?value=test&blah",
            "/?value=test&blah",
            "#place",
            "/#place",
            "?value=test&blah#place",
            "/?value=test&blah#place",
        };

        for (auto&& appendage : appendages) {
            for (auto&& [url_path, value] : find_tests) {
                std::string url_target = url_path + appendage;
                (void)url_target.data();  // Need to avoid some optimizations occuring in gcc and clang that is delaying
                                          // the creation of this data
                INFO(url_target);
                auto result = rm.process_url_target(url_target);
                REQUIRE(result.data != nullptr);
                CHECK(*result.data == value);
                CHECK(result.captures.empty());

                CHECK(result.path.size() + result.post_path.size() == url_target.size());
                if (!appendage.empty() && appendage.front() == '/') {
                    CHECK_FALSE(result.path.empty());
                    CHECK(result.path.back() == '/');

                    std::string_view adjusted_result_path{result.path};
                    adjusted_result_path.remove_suffix(1);
                    CHECK(adjusted_result_path == url_path);
                    CHECK(&*adjusted_result_path.begin() == &*url_target.begin());

                    std::string_view adjusted_appendage{appendage};
                    adjusted_appendage.remove_prefix(1);
                    CHECK(result.post_path == adjusted_appendage);
                    CHECK(&*result.post_path.end() == &*url_target.end());
                } else {
                    CHECK(result.path == url_path);
                    CHECK(&*result.path.begin() == &*url_target.begin());
                    CHECK(result.post_path == appendage);
                    CHECK(&*result.post_path.end() == &*url_target.end());
                }
            }

            for (auto&& str : not_find_tests) {
                std::string new_str = str + appendage;
                INFO(new_str);
                auto result = rm.process_url_target(new_str);
                CHECK(result.data == nullptr);
            }
        }
    }
}

TEST_CASE("route_map - captures") {
    beastmode::route_map<int> rm;
    std::stringstream         stream{};

    SECTION("Empty") {
        rm.print(stream);
        CHECK(stream.str() == "");
    }

    auto insert_and_check_result = [&rm](const auto& key, int value, std::optional<int> result_value = {}) {
        INFO("Inserting");
        //        INFO(key)
        auto result = rm.insert(key, value);
        if (result_value) {
            CHECK(*result.first == *result_value);
            CHECK_FALSE(result.second);
        } else {
            CHECK(*result.first == value);
            CHECK(result.second);
        }
    };

    insert_and_check_result("/test/" + beastmode::capture() + "/", 4);
    SECTION("Insert1") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test/{}/\" = 4\n"
              "");
    }

    insert_and_check_result(beastmode::make_path("/test2/"), 5);
    SECTION("Insert2") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test\" = nullopt\n"
              "  '2' + \"/\" = 5\n"
              "  '/' + \"{}/\" = 4\n"
              "");
    }

    insert_and_check_result("/test2/" + beastmode::capture() + "/", 6);
    SECTION("Insert3") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test\" = nullopt\n"
              "  '2' + \"/\" = 5\n"
              "    '{}' + \"/\" = 6\n"
              "  '/' + \"{}/\" = 4\n"
              "");
    }

    insert_and_check_result("/test3/" + beastmode::capture() + "/" + beastmode::capture() + "/", 7);
    SECTION("Insert4") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test\" = nullopt\n"
              "  '2' + \"/\" = 5\n"
              "    '{}' + \"/\" = 6\n"
              "  '/' + \"{}/\" = 4\n"
              "  '3' + \"/{}/{}/\" = 7\n"
              "");
    }

    insert_and_check_result("/test3/" + beastmode::capture() + "/", 8);
    SECTION("Insert4") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test\" = nullopt\n"
              "  '2' + \"/\" = 5\n"
              "    '{}' + \"/\" = 6\n"
              "  '/' + \"{}/\" = 4\n"
              "  '3' + \"/{}/\" = 8\n"
              "    '{}' + \"/\" = 7\n"
              "");
    }

    insert_and_check_result("/test/" + beastmode::capture() + "/blah", 9);
    SECTION("Insert5") {
        rm.print(stream);
        CHECK(stream.str() ==
              "'/' + \"test\" = nullopt\n"
              "  '2' + \"/\" = 5\n"
              "    '{}' + \"/\" = 6\n"
              "  '/' + \"{}/\" = 4\n"
              "    'b' + \"lah/\" = 9\n"
              "  '3' + \"/{}/\" = 8\n"
              "    '{}' + \"/\" = 7\n"
              "");
    }

    auto test_fail = [&](auto key, std::string fail_path) {
        class ExceptionMatcher : public Catch::MatcherBase<std::invalid_argument> {
        public:
            explicit ExceptionMatcher(std::string s)
                : _msg("A capture and non-capture can't be registered next to each other: " + s) {}

            bool match(const std::invalid_argument& exception) const override { return exception.what() == _msg; }

            std::string describe() const override { return _msg; }

        private:
            std::string _msg;
        };

        std::stringstream orig_stream{};
        rm.print(orig_stream);

        CHECK_THROWS_MATCHES(rm.insert(key, 1), std::invalid_argument, ExceptionMatcher(fail_path));

        std::stringstream new_stream{};
        rm.print(new_stream);

        CHECK(orig_stream.str() == new_stream.str());
    };

    SECTION("BadInserts") {
        test_fail(beastmode::make_path("/test2/test/"), "test/");
        test_fail("/test/" + beastmode::capture() + "/" + beastmode::capture(), "{}/");
        test_fail(beastmode::make_path("/test/blah"), "blah/");
        test_fail("/" + beastmode::capture(), "{}/");
        test_fail("/test2/" + beastmode::capture() + "a/", "a/");
        test_fail("/test/" + beastmode::capture() + "b/", "b/");
        test_fail("/test3/" + beastmode::capture() + "c/", "c/");
        // TODO:        test_fail("/bad/" + beastmode::capture() + beastmode::capture() + "/", "{}/");
    }

    SECTION("Find") {
        struct expected_result {
            int                      data;
            std::vector<std::string> captures;
        };

        // Find tests
        std::unordered_map<std::string, expected_result> find_tests{
            {"/test2", {5, {}}},
            {"/test2/capture_this", {6, {"capture_this"}}},
            {"/test3/capture_this", {8, {"capture_this"}}},
            {"/test3/capture_this/and_this", {7, {"capture_this", "and_this"}}},
            {"/test2/capture percent encoded", {6, {"capture percent encoded"}}},
            {"/test2/capture%20percent encoded", {6, {"capture percent encoded"}}},
            {"/test2/capture percent%20encoded", {6, {"capture percent encoded"}}},
            {"/test2/capture%20percent%20encoded", {6, {"capture percent encoded"}}},
        };

        std::vector<std::string> not_find_tests = {
            "",
            "/test",
            "/test/capture/no_path",
        };

        std::vector<std::string> appendages = {
            "",
            "/",
            "?",
            "/?",
            "#",
            "/#",
            "?#",
            "/?#",
            "?value=test&blah",
            "/?value=test&blah",
            "#place",
            "/#place",
            "?value=test&blah#place",
            "/?value=test&blah#place",
        };

        for (auto&& appendage : appendages) {
            for (auto&& [url_path, expected_result_val] : find_tests) {
                std::string url_target = url_path + appendage;
                (void)url_target.data();  // Need to avoid some optimizations occuring in gcc and clang that is delaying
                                          // the creation of this data
                INFO(url_target);
                auto result = rm.process_url_target(url_target);
                REQUIRE(result.data != nullptr);
                CHECK(*result.data == expected_result_val.data);
                CHECK(result.captures == expected_result_val.captures);

                CHECK(result.path.size() + result.post_path.size() == url_target.size());
                if (!appendage.empty() && appendage.front() == '/') {
                    CHECK_FALSE(result.path.empty());
                    CHECK(result.path.back() == '/');

                    std::string_view adjusted_result_path{result.path};
                    adjusted_result_path.remove_suffix(1);
                    CHECK(adjusted_result_path == url_path);
                    CHECK(adjusted_result_path.data() == url_target.data());

                    std::string_view adjusted_appendage{appendage};
                    adjusted_appendage.remove_prefix(1);
                    CHECK(result.post_path == adjusted_appendage);
                    CHECK(result.post_path.data() + result.post_path.size() == url_target.data() + url_target.size());
                } else {
                    CHECK(result.path == url_path);
                    CHECK(result.path.data() == url_target.data());
                    CHECK(result.post_path == appendage);
                    CHECK(result.post_path.data() + result.post_path.size() == url_target.data() + url_target.size());
                }
            }

            for (auto&& str : not_find_tests) {
                std::string new_str = str + appendage;
                INFO(new_str);
                auto result = rm.process_url_target(new_str);
                CHECK(result.data == nullptr);
                CHECK(result.captures.empty());
            }
        }
    }
}
