#include <catch2/catch.hpp>
#include <unordered_map>
#include <vector>

#include <beastmode/query_parameter.hpp>

TEST_CASE("Query Parameter Extractor") {
    std::vector<std::pair<std::string, std::unordered_map<std::string, std::string>>> tests = {
        {"?", {}},
        {"?test=5", {{"test", "5"}}},
        {"?test=5&param=value", {{"test", "5"}, {"param", "value"}}},
        {"?test=5&param=value&key=val", {{"test", "5"}, {"param", "value"}, {"key", "val"}}},
//        {"?test=5&param=value&test=different value", {{"test", "different value"}, {"param", "value"}}},
        {"?%23test=%23&param%23=val%23ue", {{"#test", "#"}, {"param#", "val#ue"}}},
        {"?%26test=%26&param%26=val%26ue", {{"&test", "&"}, {"param&", "val&ue"}}},
        {"?%3Dtest=%3D&param%3D=val%3Due", {{"=test", "="}, {"param=", "val=ue"}}},
    };

    // TODO: Multiple Values per key (what do other frameworks allow?)
    // TODO: Empty strings on either side of '&'
    // TODO: Test error cases

    std::vector<std::string> appendages = {
        "",
        "#",
        "#stuff",
        "#?",
        "#&",
        "#=",
    };

    for(const std::string& appendage : appendages) {
        for(auto&&[query_param_string, expected_query_param_map] : tests) {
            std::string new_query_param_string = query_param_string + appendage;
            INFO(new_query_param_string);

            beastmode::query_parameter_extractor extractor;
            std::optional<std::string> error = extractor.extract_query_parameters(new_query_param_string);
            CHECK(!error.has_value());
            CHECK(extractor.query_parameters_map == expected_query_param_map);
        }
    }
}
