#pragma once

#include <beastmode/server.hpp>
#include <utility>

struct with_server {
    explicit with_server(std::function<void(beastmode::server&)> setup_function, std::function<void()> execute_function)
        : _setup_function(std::move(setup_function))
        , _execute_function(std::move(execute_function))
        , _server() {
        _setup_function(_server);
        _server.start(8080);
        _server_thread = std::make_unique<std::thread>([&]() { _server.join(); });
    }

    ~with_server() {
        _server.stop();
        if (_server_thread) _server_thread->join();
    }

    void operator()() { _execute_function(); }

    std::function<void(beastmode::server&)> _setup_function;
    std::function<void()>                   _execute_function;
    beastmode::server                       _server;
    std::unique_ptr<std::thread>            _server_thread;
};
