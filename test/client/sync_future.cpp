#include <catch2/catch.hpp>

#include <beastmode/client/beastly.hpp>
#include <beastmode/client/uri.hpp>
#include <boost/asio/spawn.hpp>
#include <nlohmann/json.hpp>

//TEST_CASE("Sync Future: Async Example", "[sync_future]") {
//    boost::asio::io_context ioc;
//    boost::asio::spawn(ioc, [](boost::asio::yield_context yield) {
//        auto res = beastly::async_get("http://localhost:3000/message", yield);
//        REQUIRE(res.result() == beastly::status::ok);
//
//        auto json = nlohmann::json::parse(res.body());
//        REQUIRE(json["message"] == "Hello World!");
//    });
//    ioc.run();
//}
//
//TEST_CASE("Sync Future: Future Example", "[sync_future]") {
//    auto future_res = beastly::async_get("http://localhost:3000/message", boost::asio::use_future);
//    auto res        = future_res.get();
//    REQUIRE(res.result() == beastly::status::ok);
//
//    auto json = nlohmann::json::parse(res.body());
//    REQUIRE(json["message"] == "Hello World!");
//}
//
//template <typename... Mods>
//inline decltype(auto) sync_get(beastly::uri uri, Mods&&... mods) {
//    return beastly::async_perform_request(
//               beastly::verb::get, std::move(uri), std::forward<Mods>(mods)..., boost::asio::use_future)
//        .get();
//}
//
//TEST_CASE("Sync Future: Sync Example", "[sync_future]") {
//    auto res = sync_get("http://localhost:3000/message");
//    REQUIRE(res.result() == beastly::status::ok);
//
//    auto json = nlohmann::json::parse(res.body());
//    REQUIRE(json["message"] == "Hello World!");
//}
