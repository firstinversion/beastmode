#include <catch2/catch.hpp>

#include <beastmode/client/beastly.hpp>
#include <fmt/format.h>

TEST_CASE("Google on root domain direct", "[follow_redirect]") {
    auto r = beastly::get("https://www.google.com");
//    REQUIRE(r.result() == beastly::status::moved_permanently);
    REQUIRE(r.result() == beastly::status::ok);
    REQUIRE(r.result_int() == 200);
}