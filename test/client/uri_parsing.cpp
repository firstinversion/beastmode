#include <beastmode/client/internal/uri_grammar.hpp>
#include <beastmode/client/uri.hpp>
#include <catch2/catch.hpp>
#include <string>
#include <string_view>

#define RULE_TEST(input_value, output_type, rule)                                                                      \
    const std::string input(input_value);                                                                              \
    output_type       output;                                                                                          \
    REQUIRE(boost::spirit::qi::phrase_parse(input.begin(), input.end(), rule, skipper, output));

SCENARIO("URI Grammar Rules", "[uri_parsing]") {
    static const beastly::internal::uri_skipper<std::string::const_iterator> skipper;
    static const beastly::internal::uri_grammar<std::string::const_iterator> grammar;

    GIVEN("A bare protocol") {
        RULE_TEST("http", beastly::protocol, grammar.protocol);
        REQUIRE(output == beastly::protocol::http);
    }

    GIVEN("A terminated protocol") {
        RULE_TEST("http:", beastly::protocol, grammar.protocol);
        REQUIRE(output == beastly::protocol::http);
    }

    GIVEN("A fully terminated protocol") {
        RULE_TEST("http://", beastly::protocol, grammar.protocol);
        REQUIRE(output == beastly::protocol::http);
    }
}

SCENARIO("URI Grammar", "[uri_parsing]") {
    static const beastly::internal::uri_skipper<std::string::const_iterator> skipper;
    static const beastly::internal::uri_grammar<std::string::const_iterator> grammar;

    GIVEN("A protocol and absolute path") {
        RULE_TEST("http:/path/is/here", beastly::uri, grammar);
        REQUIRE(output.protocol() == beastly::protocol::http);
        REQUIRE(output.host().empty());
        REQUIRE(output.target() == "/path/is/here");
    }

    GIVEN("A protocol and relative path") {
        RULE_TEST("https:path/is/here", beastly::uri, grammar);
        REQUIRE(output.protocol() == beastly::protocol::https);
        REQUIRE(output.host().empty());
        REQUIRE(output.target() == "path/is/here");
    }

    GIVEN("A localhost with no port") {
        RULE_TEST("http://localhost/", beastly::uri, grammar);
        REQUIRE(output.protocol() == beastly::protocol::http);
        REQUIRE(output.host() == "localhost");
        REQUIRE(output.target() == "/");
    }

    GIVEN("A localhost with a port") {
        RULE_TEST("http://localhost:80/", beastly::uri, grammar);
        REQUIRE(output.protocol() == beastly::protocol::http);
        REQUIRE(output.host() == "localhost");
        REQUIRE(output.port() == "80");
        REQUIRE(output.target() == "/");
    }
}

TEST_CASE("URI Parsing - require storage of string uri", "[uri_parsing]") {
    std::string  sample("http://sample.com");
    beastly::uri uri(sample);
    REQUIRE(uri.str() == "http://sample.com/");
}

TEST_CASE("URI Parsing - parse http://localhost", "[uri_parsing]") {
    beastly::uri uri("http://localhost");
    REQUIRE(uri.protocol() == beastly::protocol::http);
    REQUIRE(uri.host() == "localhost");
    REQUIRE(uri.port().empty());
    REQUIRE(uri.target() == "/");
}

TEST_CASE("URI Parsing - parse http://localhost:3000", "[uri_parsing]") {
    beastly::uri uri("http://localhost:3000");
    REQUIRE(uri.protocol() == beastly::protocol::http);
    REQUIRE(uri.host() == "localhost");
    REQUIRE(uri.port() == "3000");
    REQUIRE(uri.target() == "/");
}

TEST_CASE("URI Parsing - parse https://localhost:443/login", "[uri_parsing]") {
    beastly::uri uri("https://localhost:443/login");
    REQUIRE(uri.protocol() == beastly::protocol::https);
    REQUIRE(uri.host() == "localhost");
    REQUIRE(uri.port() == "443");
    REQUIRE(uri.target() == "/login");
}

TEST_CASE("URI Parsing - parse http://apt.ubuntu.com/ubuntu", "[uri_parsing]") {
    beastly::uri uri("http://apt.ubuntu.com/ubuntu");
    REQUIRE(uri.protocol() == beastly::protocol::http);
    REQUIRE(uri.host() == "apt.ubuntu.com");
    REQUIRE(uri.port().empty());
    REQUIRE(uri.target() == "/ubuntu");
}

TEST_CASE("URI Parsing - parse https://sample.com/some/very?complicated=route&with=stuff", "[uri_parsing]") {
    beastly::uri uri("https://sample.com/some/very?complicated=route&with=stuff");
    REQUIRE(uri.protocol() == beastly::protocol::https);
    REQUIRE(uri.host() == "sample.com");
    REQUIRE(uri.port().empty());
    REQUIRE(uri.target() == "/some/very?complicated=route&with=stuff");
}
