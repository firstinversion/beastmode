#include <beastmode/client/beastly.hpp>
#include <boost/asio/spawn.hpp>
#include <catch2/catch.hpp>
#include <fstream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <string>

std::string reqres_resource(const std::string& filename) {
    std::ifstream     in("./test/resources/client/reqres_suite/" + filename);
    std::stringstream buffer;
    buffer << in.rdbuf();
    return buffer.str();
}

TEST_CASE("ReqRes - List <Resource>", "[reqres]") {
    auto r = beastly::get("https://reqres.in/api/unknown");
    REQUIRE(r.result() == beastly::status::ok);

    auto expected = nlohmann::json::parse(reqres_resource("res_list_resource.json"));
    auto actual   = nlohmann::json::parse(r.body());
    REQUIRE(expected == actual);
}

TEST_CASE("ReqRes - Async List <Resource>", "[reqres]") {
    boost::asio::io_context ioc;
    boost::asio::spawn(ioc, [](boost::asio::yield_context yield) {
        auto r = beastly::async_get("https://reqres.in/api/unknown", yield);
        REQUIRE(r.result() == beastly::status::ok);

        auto expected = nlohmann::json::parse(reqres_resource("res_list_resource.json"));
        auto actual   = nlohmann::json::parse(r.body());
        REQUIRE(expected == actual);
    });
    ioc.run();
}

TEST_CASE("ReqRes - Single <Resource>", "[reqres]") {
    auto r = beastly::get("https://reqres.in/api/unknown/2");
    REQUIRE(r.result() == beastly::status::ok);

    auto expected = nlohmann::json::parse(reqres_resource("res_single_resource.json"));
    auto actual   = nlohmann::json::parse(r.body());
    REQUIRE(expected == actual);
}

TEST_CASE("ReqRes - Single <Resource> Not Found", "[reqres]") {
    auto r = beastly::get("https://reqres.in/api/unknown/23");
    REQUIRE(r.result() == beastly::status::not_found);

    auto expected = nlohmann::json::parse(reqres_resource("res_single_resource_not_found.json"));
    auto actual   = nlohmann::json::parse(r.body());
    REQUIRE(expected == actual);
}

TEST_CASE("ReqRes - Create", "[reqres]") {
    auto r = beastly::post("https://reqres.in/api/users",
                           beastly::header{beastly::field::content_type, "application/json"},
                           beastly::sbody{(nlohmann::json{{"name", "morpheus"}, {"job", "leader"}}).dump()});
    REQUIRE(r.result() == beastly::status::created);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json.count("id") > 0);
    REQUIRE(json.count("createdAt") > 0);
    REQUIRE(json["name"] == "morpheus");
    REQUIRE(json["job"] == "leader");
}

TEST_CASE("ReqRes - Update (Put)", "[reqres]") {
    auto r = beastly::put("https://reqres.in/api/users/2",
                          beastly::header{beastly::field::content_type, "application/json"},
                          beastly::sbody{nlohmann::json{{"name", "morpheus"}, {"job", "zion resident"}}.dump()});
    REQUIRE(r.result() == beastly::status::ok);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json["name"] == "morpheus");
    REQUIRE(json["job"] == "zion resident");
    REQUIRE(json.count("updatedAt") > 0);
}

TEST_CASE("ReqRes - Update (Patch)", "[reqres]") {
    auto r = beastly::patch("https://reqres.in/api/users/2",
                            beastly::header{beastly::field::content_type, "application/json"},
                            beastly::sbody{nlohmann::json{{"name", "morpheus"}, {"job", "zion resident"}}.dump()});
    REQUIRE(r.result() == beastly::status::ok);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json["name"] == "morpheus");
    REQUIRE(json["job"] == "zion resident");
    REQUIRE(json.count("updatedAt") > 0);
}

TEST_CASE("ReqRes - Delete", "[reqres]") {
    auto r = beastly::delete_("https://reqres.in/api/users/2");
    REQUIRE(r.result() == beastly::status::no_content);
}

TEST_CASE("ReqRes - Register Successful", "[reqres]") {
    auto r =
        beastly::post("https://reqres.in/api/register",
                      beastly::header{beastly::field::content_type, "application/json"},
                      beastly::sbody{nlohmann::json{{"email", "eve.holt@reqres.in"}, {"password", "pistol"}}.dump()});
    REQUIRE(r.result() == beastly::status::ok);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json.count("id") > 0);
    REQUIRE(json.count("token") > 0);
}

TEST_CASE("ReqRes - Register Unsuccessful", "[reqres]") {
    auto r = beastly::post("https://reqres.in/api/register",
                           beastly::header{beastly::field::content_type, "application/json"},
                           beastly::sbody{reqres_resource("req_register_unsuccessful.json")});
    REQUIRE(r.result() == beastly::status::bad_request);

    auto expected = nlohmann::json::parse(reqres_resource("res_register_unsuccessful.json"));
    auto actual   = nlohmann::json::parse(r.body());
    REQUIRE(expected == actual);
}

TEST_CASE("ReqRes - Login Successful", "[reqres]") {
    auto r = beastly::post(
        "https://reqres.in/api/login",
        beastly::header{beastly::field::content_type, "application/json"},
        beastly::sbody{nlohmann::json{{"email", "eve.holt@reqres.in"}, {"password", "cityslicka"}}.dump()});
    REQUIRE(r.result() == beastly::status::ok);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json.count("token") > 0);
}

TEST_CASE("ReqRes - Login Unsuccessful", "[reqres]") {
    auto r = beastly::post("https://reqres.in/api/login",
                           beastly::header{beastly::field::content_type, "application/json"},
                           beastly::sbody{reqres_resource("req_login_unsuccessful.json")});
    REQUIRE(r.result() == beastly::status::bad_request);

    auto expected = nlohmann::json::parse(reqres_resource("res_login_unsuccessful.json"));
    auto actual   = nlohmann::json::parse(r.body());
    REQUIRE(expected == actual);
}

TEST_CASE("ReqRes - Delayed Response", "[reqres]") {
    auto r = beastly::get("https://reqres.in/api/users?delay=3");
    REQUIRE(r.result() == beastly::status::ok);

    auto json = nlohmann::json::parse(r.body());
    REQUIRE(json["page"] == 1);
    REQUIRE(json["total_pages"] == 2);
    REQUIRE(json.count("data") > 0);
}
