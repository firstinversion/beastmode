#include <beastmode/client/beastly.hpp>
#include <boost/asio/spawn.hpp>
#include <catch2/catch.hpp>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <string>

std::string local_resource(const std::string& filename) {
    std::ifstream     in("./test/resources/client/local_suite/" + filename);
    std::stringstream buffer;
    buffer << in.rdbuf();
    return buffer.str();
}

//TEST_CASE("Local - Sync List Users", "[local]") {
//    auto r = beastly::get("http://localhost:3000/api/users");
//    REQUIRE(r.result() == beastly::status::ok);
//
//    auto expected = nlohmann::json::parse(local_resource("local_list_users.json"));
//    auto actual   = nlohmann::json::parse(r.body());
//    REQUIRE(expected == actual);
//}
//
//TEST_CASE("Local - Sync Single User", "[local]") {
//    auto r = beastly::get("http://localhost:3000/api/users/2");
//    REQUIRE(r.result() == beastly::status::ok);
//
//    auto expected = nlohmann::json::parse(local_resource("local_single_user.json"));
//    auto actual   = nlohmann::json::parse(r.body());
//    REQUIRE(expected == actual);
//}
//
//TEST_CASE("Local - Sync Single User Not Found", "[local]") {
//    auto r = beastly::get("http://localhost:3000/api/users/2487");
//    REQUIRE(r.result() == beastly::status::not_found);
//}
//
//TEST_CASE("Local - Async List Users", "[local]") {
//    boost::asio::io_context ioc;
//    boost::asio::spawn(ioc, [](boost::asio::yield_context yield) {
//        auto r = beastly::async_get("http://localhost:3000/api/users", yield);
//        REQUIRE(r.result() == beastly::status::ok);
//
//        auto expected = nlohmann::json::parse(local_resource("local_list_users.json"));
//        auto actual   = nlohmann::json::parse(r.body());
//        REQUIRE(expected == actual);
//    });
//    ioc.run();
//}
