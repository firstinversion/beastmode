#include <catch2/catch.hpp>

#include <beastmode/client/beastly.hpp>
#include <beastmode/endpoint.hpp>
#include <beastmode/server.hpp>

#include "./with_server.hpp"

class echo_endpoint : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get() { response().body() = "echo"; }

    void put() { response().body() = request().body(); }
};

class static_value : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get() { response().body() = _static_value; }

    void put() { _static_value = request().body(); }

private:
    static inline std::string _static_value = "";
};

void setup_with_server(beastmode::server& server) {
    server.add_endpoint<echo_endpoint>("/echo");
    server.add_endpoint<static_value>("/static_value");
}

TEST_CASE("construct", "[with_server]") {
    with_server(&setup_with_server, []() {})();
}

TEST_CASE("echo get", "[with_server]") {
    with_server(&setup_with_server, []() {
        auto r = beastly::get("http://localhost:8080/echo");
        REQUIRE(r.body() == "echo");
    })();
}

TEST_CASE("echo put", "[with_server]") {
    with_server(&setup_with_server, []() {
        auto r = beastly::put("http://localhost:8080/echo", beastly::sbody("some message"));
        REQUIRE(r.body() == "some message");
    })();
}

TEST_CASE("static value", "[with_server]") {
    with_server(&setup_with_server, []() {
        auto r1 = beastly::put("http://localhost:8080/static_value", beastly::sbody("some message"));
        REQUIRE(r1.result() == boost::beast::http::status::ok);
        auto r2 = beastly::get("http://localhost:8080/static_value");
        REQUIRE(r2.body() == "some message");
    })();
}
