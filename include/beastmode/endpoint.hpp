#pragma once

#include "boost_aliases.hpp"
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace beastmode {

    class endpoint {
    public:
        endpoint(const http::request<http::string_body>& req, http::response<http::string_body>& res)
            : _req(req)
            , _res(res) {
            // Nothing
        }

        [[nodiscard]] const auto& request() const { return _req; }

        [[nodiscard]] auto& response() const { return _res; }

        void get() { _method_not_supported(); }

        void post() { _method_not_supported(); }

        void put() { _method_not_supported(); }

        void delete_() { _method_not_supported(); }

        void options() { _method_not_supported(); }

    private:
        void _method_not_supported() {
            _res.base().result(http::status::not_found);
            _res.body() =
                fmt::format("The resource '{}' does not support the method '{}'", _req.target(), _req.method_string());
        }

    private:
        const http::request<http::string_body>& _req;
        http::response<http::string_body>&      _res;
    };

}  // namespace beastmode
