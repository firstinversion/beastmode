#pragma once

#include "capture_parameter.hpp"

namespace beastmode {

    struct path_component_string {
        std::string component;
    };

    struct path_component_anonymous_capture {};

    struct path_component_capture_target {};

    // template<class Endpoint, class T>
    // struct capture_target {
    //
    //};

    template <class... PathComponents>
    class path {
        static_assert(sizeof...(PathComponents) != 0);

    private:
        explicit path(std::tuple<PathComponents...> components)
            : _components{components} {
            // Nothing
        }

        explicit path(PathComponents... components)
            : _components{components...} {
            // Nothing
        }

    public:
        template <size_t Idx>
        [[nodiscard]] std::optional<std::string_view> get_component_string_view() const {
            using ComponentType = std::tuple_element_t<Idx, std::tuple<PathComponents...>>;
            if constexpr (std::is_same_v<ComponentType, path_component_string>) {
                return std::make_optional<std::string_view>(std::get<Idx>(_components).component);
            } else {
                return std::nullopt;
            }
        }

    private:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-redundant-declaration"
        template <class... LhsPathComponents, class... RhsPathComponents>
        friend auto operator+(const path<LhsPathComponents...>& lhs, const path<RhsPathComponents...>& rhs)
            -> path<LhsPathComponents..., RhsPathComponents...>;

        template <class... LhsPathComponents>
        friend auto operator+(const path<LhsPathComponents...>& lhs, const std::string& rhs)
            -> path<LhsPathComponents..., path_component_string>;

        template <class... RhsPathComponents>
        friend auto operator+(const std::string& lhs, const path<RhsPathComponents...>& rhs)
            -> path<path_component_string, RhsPathComponents...>;
#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-template-friend"
        friend auto capture();
        friend auto make_path(const std::string&);
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

    private:
        std::tuple<PathComponents...> _components;
    };

    template <class... LhsPathComponents, class... RhsPathComponents>
    auto operator+(const path<LhsPathComponents...>& lhs, const path<RhsPathComponents...>& rhs)
        -> path<LhsPathComponents..., RhsPathComponents...> {
        return path<LhsPathComponents..., RhsPathComponents...>{std::tuple_cat(lhs._components, rhs._components)};
    }

    template <class... LhsPathComponents>
    auto operator+(const path<LhsPathComponents...>& lhs, const std::string& rhs)
        -> path<LhsPathComponents..., path_component_string> {
        return lhs + path<path_component_string>{path_component_string{rhs}};
    }

    template <class... RhsPathComponents>
    auto operator+(const std::string& lhs, const path<RhsPathComponents...>& rhs)
        -> path<path_component_string, RhsPathComponents...> {
        return path<path_component_string>{path_component_string{lhs}} + rhs;
    }

    // TODO: Move versions of operator+ with string

    // template<class Endpoint, class T>
    // auto capture(capture_parameter<T> Endpoint::* param) {
    //    return path{path_component_capture_target{}};
    //}

    inline auto capture() { return path{path_component_anonymous_capture{}}; }

    // TODO: Should this just be a constructor?
    inline auto make_path(const std::string& str) { return path{path_component_string{str}}; }

}  // namespace beastmode
