#pragma once

#include <any>

namespace beastmode {

    class resource_base {
    public:
        //        resource_base(const boost::beast::http::request<boost::beast::http::string_body>& req,
        //        boost::beast::http::response<boost::beast::http::string_body>& res) {} void operator()() {}
    };

    class resource {
    public:
        resource()
            : _is_ptr(false)
            , _resource() {}

        template <class T>
        explicit resource(T* resource)
            : _is_ptr(true)
            , _resource(resource) {}

        template <class ValueType, class... Args>
        decltype(auto) emplace(Args&&... args) {
            return _resource.emplace<ValueType>(std::forward<Args>(args)...);
        }

        template <class T>
        T& get() {
            if (_is_ptr) {
                return *std::any_cast<T*>(_resource);
            } else {
                return *std::any_cast<T>(&_resource);
            }
        }

    private:
        bool     _is_ptr;
        std::any _resource;
    };

}  // namespace beastmode
