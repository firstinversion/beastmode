#pragma once

#include <string>

namespace beastmode {

    char        decode_percent_encoded_char(const char encoded_chars[3]);
    std::string decode_percent_endoded_string(const std::string&);

}  // namespace beastmode
