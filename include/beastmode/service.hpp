#pragma once

#include "boost_aliases.hpp"

namespace beastmode {

    class service {
    public:
        service(const http::request<http::string_body>& req, http::response<http::string_body>& res)
            : _req(req)
            , _res(res) {
            // Nothing
        }

        const auto& request() const { return _req; }

        auto& response() const { return _res; }

    private:
        const http::request<http::string_body>& _req;
        http::response<http::string_body>&      _res;
    };

}  // namespace beastmode
