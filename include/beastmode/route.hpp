#pragma once

#include <boost/beast.hpp>
#include <boost/container_hash/hash.hpp>
#include <string>

#include "boost_aliases.hpp"

namespace beastmode {

    struct route {
        std::string path;
        http::verb  verb;

        bool operator==(const route& rhs) const { return path == rhs.path && verb == rhs.verb; }
    };

}  // namespace beastmode

// TODO: Perhaps have a map of paths to arrays, rather than hashing in the verb as well?
namespace std {

    template <>
    struct hash<beastmode::route> {
        size_t operator()(const beastmode::route& route) const noexcept {
            size_t seed = 0;
            boost::hash_combine(seed, route.path);
            boost::hash_combine(seed, route.verb);
            return seed;
        }
    };

}  // namespace std
