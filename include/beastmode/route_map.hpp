#pragma once

#include <cassert>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "path.hpp"
#include "percent_encoding.hpp"

// TODO: Iterators?
namespace beastmode {

    template <class T>
    class route_map {
    private:
        class path_search_state;
        class url_string_search_state;

        struct node;

    public:
        route_map()
            : _first_node(nullptr) {}
        route_map(const route_map&)     = default;
        route_map(route_map&&) noexcept = default;

    public:
        template <class... PathComponents>
        std::pair<T*, bool> insert(const path<PathComponents...>& key, const T& value);
        template <class... PathComponents>
        std::pair<T*, bool> insert(const path<PathComponents...>& key, T&& value);
        template <class... PathComponents, class... Args>
        std::pair<T*, bool> emplace(const path<PathComponents...>& key, Args&&... args);
        template <class Stream>
        void print(Stream& stream);

    public:
        struct url_process_result;
        url_process_result process_url_target(const std::string& target);

    private:
        template <class... PathComponents, class... Args>
        std::pair<T*, bool> emplace_impl(const path<PathComponents...>& key, Args&&... args);
        node*               find_node(std::string_view path);

    private:
        std::unique_ptr<node> _first_node;
    };

    template <class T>
    struct route_map<T>::url_process_result {
        T*                       data;
        std::string_view         path;
        std::string_view         post_path;
        std::vector<std::string> captures;
    };

    template <class T>
    auto route_map<T>::process_url_target(const std::string& target) -> url_process_result {
        if (_first_node) {
            url_string_search_state state(target);
            auto                    result = _first_node->find_closest_node(state);
            // TODO: Clean this up
            if (result.search_state_finished &&
                *result.compression_string_mismatch_pos == result.closest_node->_compression_str.size()) {
                const char*      front_of_target = target.data();
                const char*      end_of_path     = state.end_of_path();
                const char*      end_of_target   = target.data() + target.size();
                std::string_view path{front_of_target, static_cast<size_t>(end_of_path - front_of_target)};
                std::string_view post_path{end_of_path, static_cast<size_t>(end_of_target - end_of_path)};
                return {result.closest_node->_data.get(), path, post_path, state.captures()};
            }
        }

        // TODO: What should this look like?
        return {nullptr, {}, {}, {}};
    }

    template <class T>
    template <class... PathComponents>
    std::pair<T*, bool> route_map<T>::insert(const path<PathComponents...>& key, const T& value) {
        return emplace_impl(key, value);
    }

    template <class T>
    template <class... PathComponents>
    std::pair<T*, bool> route_map<T>::insert(const path<PathComponents...>& key, T&& value) {
        return emplace_impl(key, std::move(value));
    }

    template <class T>
    template <class... PathComponents, class... Args>
    std::pair<T*, bool> route_map<T>::emplace(const path<PathComponents...>& key, Args&&... args) {
        return emplace_impl(key, std::forward<Args>(args)...);
    }

    template <class T>
    template <class... PathComponents, class... Args>
    std::pair<T*, bool> route_map<T>::emplace_impl(const path<PathComponents...>& key, Args&&... args) {
        T*   data     = nullptr;
        bool inserted = false;

        auto create_data = [&data, &inserted, &args...]() {
            data     = new T{std::forward<Args>(args)...};
            inserted = true;
            return data;
        };

        auto create_node_from_state = [&args...](path_search_state& state, node* next_node, node* child_node, T* data) {
            return new node{
                state.current_character(), next_node, child_node, state.get_string_from_remaining_path(), data};
        };

        auto create_node_from_compression_str =
            [](std::string& compression_str, size_t pos, node* next_node, node* child_node, T* data) {
                auto* new_node = new node{compression_str[pos],
                                          next_node,
                                          child_node,
                                          compression_str.substr(pos + 1, compression_str.length() - pos - 1),
                                          data};
                compression_str.resize(pos);
                return new_node;
            };

        auto throw_exception = [](path_search_state& state) {
            char c        = state.current_character();
            auto temp_msg = std::string{"A capture and non-capture can't be registered next to each other: "} + c +
                            state.get_string_from_remaining_path();
            std::string msg;
            for (char c : temp_msg) {
                if (c == 0) {
                    msg.append("{}");
                } else {
                    msg.push_back(c);
                }
            }
            throw std::invalid_argument(msg);
        };

        path_search_state state{key};
        if (_first_node) {
            auto result = _first_node->find_closest_node(state);
            if (result.compression_string_mismatch_pos) {
                size_t pos = *result.compression_string_mismatch_pos;
                if (pos < result.closest_node->_compression_str.size()) {
                    // We need to delay new child node creation, since we could throw below
                    auto create_new_child_node = [&]() {
                        // Create new child node from existing path
                        return create_node_from_compression_str(result.closest_node->_compression_str,
                                                                pos,
                                                                nullptr,
                                                                result.closest_node->_child_node.release(),
                                                                result.closest_node->_data.release());
                    };

                    if (result.search_state_finished) {
                        // Modify this node
                        result.closest_node->_child_node.reset(create_new_child_node());
                        result.closest_node->_data.reset(create_data());
                    } else {
                        if (result.closest_node->_compression_str[pos] == 0 || state.current_character() == 0) {
                            throw_exception(state);
                        }

                        if (pos == 0) {
                            if (result.closest_node->_edge == 0) {
                                throw_exception(state);
                            }
                        } else {
                            if (result.closest_node->_compression_str[pos - 1] == 0) {
                                throw_exception(state);
                            }
                        }

                        // Make the new child node for this path
                        node* additional_new_child_node =
                            create_node_from_state(state, create_new_child_node(), nullptr, create_data());

                        // Modify this node
                        result.closest_node->_child_node.reset(additional_new_child_node);
                        result.closest_node->_data.reset();
                    }
                } else {
                    if (result.search_state_finished) {
                        if (result.closest_node->_data) {
                            data = result.closest_node->_data.get();
                        } else {
                            result.closest_node->_data.reset(create_data());
                        }
                    } else {
                        result.closest_node->_child_node.reset(
                            create_node_from_state(state, nullptr, nullptr, create_data()));
                    }
                }
            } else {
                // If the closest_node is capturing on it's first character, we can't add another node
                if (result.closest_node->_edge == 0 || state.current_character() == 0) {
                    throw_exception(state);
                }

                // Setup the next_node
                result.closest_node->_next_node.reset(create_node_from_state(state, nullptr, nullptr, create_data()));
            }
        } else {
            _first_node.reset(create_node_from_state(state, nullptr, nullptr, create_data()));
        }

        return {data, inserted};
    }

    template <class T>
    template <class Stream>
    void route_map<T>::print(Stream& stream) {
        if (_first_node) {
            _first_node->print(stream, 0);
        }
    }

    template <class T>
    typename route_map<T>::node* route_map<T>::find_node(std::string_view path) {
        if (_first_node) {
            url_string_search_state state(path);
            auto                    result = _first_node->find_closest_node(state);
            if (result.search_state_finished &&
                *result.compression_string_mismatch_pos == result.node->_compression_str.size()) {
                return result.node;
            }
        }
        return nullptr;
    }

    template <class T>
    class route_map<T>::path_search_state {
    public:
        template <class... PathComponents>
        explicit path_search_state(const path<PathComponents...>& path)
            : path_search_state(path, std::make_index_sequence<sizeof...(PathComponents)>()) {
            // Nothing
        }

    private:
        template <class... PathComponents, size_t... Idxs>
        path_search_state(const path<PathComponents...>& path, std::index_sequence<Idxs...>)
            : _path({path.template get_component_string_view<Idxs>()...})
            , _path_it(_path.begin())
            , _current_character(0) {
            assert(!_path.empty());  // TODO: Is this right?
            advance();
        }

    public:
        bool capture_during_search() const { return false; }

        bool match(char c) const { return c == current_character(); }

        char current_character() const { return _current_character; }

        bool finished() const { return _path_it == _path.end() && _current_character == '/'; }

        void advance() {
            assert(!finished());
            if (_path_it == _path.end()) {
                _current_character = '/';
            } else {
                std::optional<std::string_view>& path_segment = *_path_it;
                if (path_segment) {
                    _current_character = path_segment->front();
                    path_segment->remove_prefix(1);
                    if (path_segment->empty()) {
                        ++_path_it;
                    }
                } else {
                    _current_character = 0;
                    ++_path_it;
                }
            }
        }

        std::string get_string_from_remaining_path() {
            std::string s;
            while (!finished()) {
                advance();
                s.push_back(current_character());
            }
            return s;
        }

    private:
        using NormalizedPath = std::vector<std::optional<std::string_view>>;
        NormalizedPath           _path;
        NormalizedPath::iterator _path_it;
        char                     _current_character;
    };

    template <class T>
    class route_map<T>::url_string_search_state {
    public:
        explicit url_string_search_state(std::string_view path)
            : _path(path)
            , _current_character(0)
            , _capturing(false) {
            advance();
        }

        bool capture_during_search() const { return true; }

        bool match(char c) {
            if (_capturing) {
                assert(!_captures.empty());
                std::string& capture = _captures.back();
                assert(capture.size() == 1);
                while (current_character() != c) {
                    capture.push_back(current_character());
                    if (finished()) {
                        return false;
                    }
                    advance();
                }
                _capturing = false;
                return true;
            } else {
                if (c == 0) {
                    _capturing = true;
                    _captures.emplace_back(1, current_character());
                    return true;
                } else {
                    return c == current_character();
                }
            }
        }

        char current_character() const { return _current_character; }

        bool finished() const { return _path.length() == 0 && _current_character == '/'; }

        const char* end_of_path() const { return _path.data() + _path.length(); }

        void advance() {
            assert(!finished());
            if (_path.length() == 0) {
                _current_character = '/';
            } else {
                if (_path.front() == '%') {
                    if (_path.length() < 3) {
                        // TODO: Malformed URI
                    }
                    _current_character = decode_percent_encoded_char(_path.data());
                    _path.remove_prefix(3);
                } else {
                    _current_character = _path.front();
                    _path.remove_prefix(1);
                }
                if (!_path.empty()) {
                    if (_path.front() == '?' || _path.front() == '#') {
                        _path = _path.substr(0, 0);
                    }
                }
            }
        }

        // TODO: Can we make this not need to copy out
        // Maybe a reference passed in on construction?
        std::vector<std::string> captures() { return _captures; }

    private:
        std::string_view         _path;
        char                     _current_character;
        bool                     _capturing;
        std::vector<std::string> _captures;
    };

    template <class T>
    struct route_map<T>::node {
        char                  _edge;
        std::unique_ptr<node> _next_node;
        std::unique_ptr<node> _child_node;
        std::string           _compression_str;
        std::unique_ptr<T>    _data;

        node(char edge, node* next_node, node* child_node, const std::string& compression_str, T* data)
            : _edge(edge)
            , _next_node(next_node)
            , _child_node(child_node)
            , _compression_str(compression_str)
            , _data(data) {
            // Nothing
        }

        struct search_result {
            node*                 closest_node;
            std::optional<size_t> compression_string_mismatch_pos;
            bool                  search_state_finished;
        };

        template <class SearchState>
        search_result find_closest_node(SearchState& search_state) {
            if (search_state.match(_edge)) {
                // Compare to the compression string, if it doesn't match then this path isn't registered
                for (size_t i = 0; i < _compression_str.size(); i++) {
                    if (search_state.finished()) {
                        return search_result{this, i, true};
                    }
                    search_state.advance();
                    if (!search_state.match(_compression_str[i])) {
                        return search_result{this, i, false};
                    }
                }

                if (search_state.finished()) {
                    return search_result{this, _compression_str.size(), true};
                }

                search_state.advance();
                if (_child_node) {
                    return _child_node->find_closest_node(search_state);
                } else {
                    return search_result{this, _compression_str.size(), false};
                }
            } else {
                if (_next_node) {
                    return _next_node->find_closest_node(search_state);
                }
                return search_result{this, std::nullopt, false};
            }
        }

        template <class Stream>
        void print(Stream& stream, size_t indent) {
            for (int i = 0; i < indent; i++) {
                stream << "  ";
            }

            if (_edge == 0) {
                stream << "'{}' + \"";
            } else {
                stream << "'" << _edge << "' + \"";
            }

            for (char c : _compression_str) {
                if (c == 0) {
                    stream << "{}";
                } else {
                    stream << c;
                }
            }
            stream << "\" = ";

            if (_data) {
                stream << *_data;
            } else {
                stream << "nullopt";
            }

            stream << '\n';

            if (_child_node) {
                _child_node->print(stream, indent + 1);
            }

            if (_next_node) {
                _next_node->print(stream, indent);
            }
        }
    };

}  // namespace beastmode
