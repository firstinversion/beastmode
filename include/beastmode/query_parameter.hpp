#pragma once

#include <optional>

#include "beastmode/percent_encoding.hpp"
#include "beastmode/resource.hpp"
#include "beastmode/service.hpp"

namespace beastmode {
    struct post_path : resource_base {
        std::string_view sv;
    };

    struct query_parameter_extractor {
        std::optional<std::string> extract_query_parameters(std::string_view query_params_sv) {
            if(query_params_sv.empty()) {
                return std::nullopt;
            }

            if(query_params_sv.front() != '?') {
                if(query_params_sv.front() != '#') {
                    return {"First post path character was not '#' or '?'.  This shouldn't be possible."};
                } else {
                    return std::nullopt;
                }
            }

            std::optional<std::string> key;
            std::optional<std::string> value;

            for(int i = 1; i < query_params_sv.length(); i++) {
                char current_character = query_params_sv[i];

                if(current_character == '#') {
                    break;
                }

                char effective_character = current_character;
                if(current_character == '%') {
                    if(query_params_sv.length() - i < 3) {
                        return {"Encountered percent encoding, but not enough characters remain to decode"};
                    }
                    effective_character = decode_percent_encoded_char(&query_params_sv[i]);
                    i += 2;
                }

                if(!key) {
                    key.emplace();
                }

                if(!value) {
                    if(current_character == '=') {
                        value.emplace();
                    } else {
                        key->push_back(effective_character);
                    }
                } else {
                    if(current_character == '&') {
                        query_parameters_map.try_emplace(*key, *value);
                        key.reset();
                        value.reset();
                    } else {
                        value->push_back(effective_character);
                    }
                }
            }

            if(key && value) {
                query_parameters_map.try_emplace(*key, *value);
                return std::nullopt;
            } else if(key || value) {
                return {"Failed to parse out a final key and value"};
            } else {
                return std::nullopt;
            }
        }

        std::unordered_map<std::string, std::string> query_parameters_map;
    };

    class query_parameters : service {
    public:
        using service::service;

        const auto& params() const {
            return _query_parameters.query_parameters_map;
        }

        bool operator()(const post_path& post_path) {
            if(auto error = _query_parameters.extract_query_parameters(post_path.sv); error) {
                response().result(http::status::internal_server_error);
                response().body() = *error;
                return false;
            }
            return true;
        }

    private:
        query_parameter_extractor _query_parameters;
    };
}
