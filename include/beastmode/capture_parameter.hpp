#pragma once

#include <optional>

namespace beastmode {

    template <size_t IDX, class T = std::string>
    class capture_parameter {
        using ParamType = T;

    public:
        capture_parameter() = default;
        capture_parameter(const T& value)
            : _value(value) {}
        capture_parameter(T&& value)
            : _value(std::move(value)) {}
        T& operator*() { return *_value; }

    private:
        std::optional<T> _value;
    };

}  // namespace beastmode
