#pragma once

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/status.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <string_view>

#include <beastmode/client/internal/async.hpp>
#include <beastmode/client/internal/handle_modifier.hpp>
#include <beastmode/client/internal/sync.hpp>
#include <beastmode/client/uri.hpp>

namespace beastly {

    using boost::beast::http::field;
    using boost::beast::http::status;
    using boost::beast::http::verb;

    using boost::beast::http::message;
    using boost::beast::http::request;
    using boost::beast::http::request_header;
    using boost::beast::http::response;
    using boost::beast::http::response_header;
    using boost::beast::http::string_body;

}  // namespace beastly
