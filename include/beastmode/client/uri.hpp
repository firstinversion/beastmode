#pragma once

#include <string>

namespace beastly {

    enum class protocol { http, https };

    class uri {
    public:
        uri() = default;
        uri(const std::string& uri);
        uri(const char* uri);
        uri(std::string_view uri);

        uri(const uri&)     = default;
        uri(uri&&) noexcept = default;
        uri& operator=(const uri&) = default;
        uri& operator=(uri&&) = default;

        std::string str() const;

#include <beastmode/client/internal/macro.hpp>

        GET_SET_VAL(uri, beastly::protocol, protocol);
        GET_SET_REF(uri, std::string, host);
        GET_SET_REF(uri, std::string, port);
        GET_SET_REF(uri, std::string, target);

#include <beastmode/client/internal/unmacro.hpp>

    private:
        beastly::protocol _protocol;
        std::string       _host;
        std::string       _port;
        std::string       _target;
    };

}  // namespace beastly
