#pragma once

#include <boost/beast/http/field.hpp>
#include <string>
#include <utility>

namespace beastly {

    class sbody {
    public:
        explicit sbody(std::string body)
            : _body(std::move(body)) {}

        [[nodiscard]] const std::string& body() const { return _body; }

    private:
        std::string _body;
    };

    class header {
    public:
        header(boost::beast::http::field f, std::string value)
            : _field(f)
            , _value(std::move(value)) {}

        [[nodiscard]] boost::beast::http::field field() const { return _field; }

        [[nodiscard]] const std::string& value() const { return _value; }

    private:
        boost::beast::http::field _field;
        std::string               _value;
    };

}  // namespace beastly
