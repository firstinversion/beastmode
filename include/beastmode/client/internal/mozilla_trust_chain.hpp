#pragma once

#include <string>

namespace beastly::internal {

    extern const char* mozilla_trust_chain_part1;
    extern const char* mozilla_trust_chain_part2;
    extern const char* mozilla_trust_chain_part3;
    extern const char* mozilla_trust_chain_part4;

    extern const std::string mozilla_trust_chain;

}  // namespace beastly::internal
