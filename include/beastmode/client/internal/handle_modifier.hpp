#pragma once

#include <beastmode/client/modifiers.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>

namespace beastly::internal {

    template <typename T>
    inline void handle_modifier(boost::beast::http::request<boost::beast::http::string_body>& req, T&& mod);

    template <>
    inline void handle_modifier<beastly::header>(boost::beast::http::request<boost::beast::http::string_body>& req,
                                                 header&&                                                      mod) {
        req.set(mod.field(), mod.value());
    }

    template <>
    inline void handle_modifier<beastly::sbody>(boost::beast::http::request<boost::beast::http::string_body>& req,
                                                sbody&&                                                       mod) {
        req.body() = mod.body();
        req.prepare_payload();
    }

}  // namespace beastly::internal
