#pragma once

#include <beastmode/client/internal/async.hpp>
#include <beastmode/client/uri.hpp>
#include <boost/asio/use_future.hpp>
#include <boost/beast/http/verb.hpp>

namespace beastly {

    template <typename... Mods>
    inline decltype(auto) perform_request(boost::beast::http::verb v, uri uri, Mods&&... mods) {
        return async_perform_request(v, std::move(uri), std::forward<Mods>(mods)..., boost::asio::use_future).get();
    }

#define VERB_FUNCTION(name)                                                                                            \
    template <typename... Mods>                                                                                        \
    inline decltype(auto) name(uri uri, Mods&&... mods) {                                                              \
        return perform_request(boost::beast::http::verb::name, std::move(uri), std::forward<Mods>(mods)...);           \
    }

    VERB_FUNCTION(get)
    VERB_FUNCTION(post)
    VERB_FUNCTION(put)
    VERB_FUNCTION(patch)
    VERB_FUNCTION(delete_)

#undef VERB_FUNCTION

}  // namespace beastly
