#pragma once

#include <beastmode/client/internal/async_implementation.hpp>
#include <beastmode/client/internal/handle_modifier.hpp>
#include <beastmode/client/uri.hpp>
#include <boost/asio/compose.hpp>
#include <boost/beast/http/field.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/verb.hpp>
#include <boost/beast/version.hpp>

namespace beastly::internal {

    inline void async_handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& /*req*/) {
        // no-op
    }

    template <typename Token>
    inline decltype(auto) async_handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& /*req*/,
                                                 Token&& token) {
        return std::forward<Token>(token);
    }

    template <typename Mod, typename... ModsAndToken>
    inline decltype(auto) async_handle_modifiers(boost::beast::http::request<boost::beast::http::string_body>& req,
                                                 Mod&& mod, ModsAndToken&&... mods_and_token) {
        handle_modifier(req, std::forward<Mod>(mod));
        return async_handle_modifiers(req, std::forward<ModsAndToken>(mods_and_token)...);
    }

}  // namespace beastly::internal

namespace beastly {

    template <typename... ModsAndToken>
    inline decltype(auto) async_perform_request(boost::beast::http::verb v, beastly::uri uri,
                                                ModsAndToken&&... mods_and_token) {
        boost::beast::http::request<boost::beast::http::string_body> req{v, uri.target(), 11};
        req.set(boost::beast::http::field::host, uri.host());
        req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);

        auto&& token = internal::async_handle_modifiers(req, std::forward<ModsAndToken>(mods_and_token)...);
        if (uri.protocol() == protocol::http) {
            return boost::asio::async_compose<decltype(token),
                                              void(boost::system::error_code,
                                                   boost::beast::http::response<boost::beast::http::string_body>)>(
                internal::async_perform_request_impl<false, decltype(token)>(std::move(uri), std::move(req), token),
                token);
        } else {
            return boost::asio::async_compose<decltype(token),
                                              void(boost::system::error_code,
                                                   boost::beast::http::response<boost::beast::http::string_body>)>(
                internal::async_perform_request_impl<true, decltype(token)>(std::move(uri), std::move(req), token),
                token);
        }
    }

#define VERB_FUNCTION(name)                                                                                            \
    template <typename... ModsAndToken>                                                                                \
    inline decltype(auto) async_##name(uri uri, ModsAndToken&&... mods_and_token) {                                    \
        return async_perform_request(                                                                                  \
            boost::beast::http::verb::name, std::move(uri), std::forward<ModsAndToken>(mods_and_token)...);            \
    }

    VERB_FUNCTION(get)
    VERB_FUNCTION(post)
    VERB_FUNCTION(put)
    VERB_FUNCTION(patch)
    VERB_FUNCTION(delete_)

#undef VERB_FUNCTION

}  // namespace beastly
