#pragma once

#include <beastmode/client/uri.hpp>
#include <boost/phoenix/phoenix.hpp>
#include <boost/spirit/include/qi.hpp>

namespace beastly::internal {

    namespace ascii = boost::spirit::ascii;
    namespace phx   = boost::phoenix;
    namespace qi    = boost::spirit::qi;
    using namespace qi::labels;

    template <typename Iterator>
    struct uri_skipper : qi::grammar<Iterator> {
        uri_skipper()
            : uri_skipper::base_type(skip) {
            skip = ascii::space;
        }

        qi::rule<Iterator> skip;
    };

    template <typename Iterator, typename Skipper = uri_skipper<Iterator>>
    struct uri_grammar : qi::grammar<Iterator, uri(), Skipper> {
        uri_grammar()
            : uri_grammar::base_type(uri_part) {
            protocol_symbols.add("http", beastly::protocol::http)("https", beastly::protocol::https);
            protocol %= protocol_symbols;

            host %= qi::lexeme[*(qi::char_ - '/' - ':')];

            port %= ':' > qi::lexeme[+qi::digit];

            target %= qi::lexeme[*qi::char_];

            authority =
                qi::lit("//") > host[phx::bind(&uri::set_host, _r1, _1)] > -port[phx::bind(&uri::set_port, _r1, _1)];

            uri_part = protocol[phx::bind(&uri::set_protocol, _val, _1)] > ':' > -authority(_val) >
                  target[phx::bind(&uri::set_target, _val, _1)];
        }

        qi::symbols<char, beastly::protocol> protocol_symbols;

        qi::rule<Iterator, beastly::protocol(), Skipper> protocol;
        qi::rule<Iterator, std::string(), Skipper>       host;
        qi::rule<Iterator, std::string(), Skipper>       port;
        qi::rule<Iterator, std::string(), Skipper>       target;
        qi::rule<Iterator, void(uri&), Skipper>          authority;
        qi::rule<Iterator, uri(), Skipper>               uri_part;
    };

}  // namespace beastly::internal
