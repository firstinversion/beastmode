#pragma once

#include <boost/asio/ip/basic_resolver.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http/impl/status.ipp>
#include <boost/beast/http/read.hpp>
#include <boost/beast/http/status.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/write.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>
#include <boost/system/error_code.hpp>

#include <beastmode/client/internal/mozilla_trust_chain.hpp>
#include <beastmode/client/uri.hpp>

namespace beastly::internal {

    inline const char* use_port_with_default(const beastly::uri& uri) {
        if (!uri.port().empty()) return uri.port().data();
        switch (uri.protocol()) {
        case protocol::http: return "80";
        case protocol::https: return "443";
        }
        // Unreachable, but suppresses windows' warnings
        return nullptr;
    }

    inline bool is_ssl_short_read(const boost::beast::error_code& ec) {
        if (ec.value() != 335544539) return false;
        if (std::string_view{ec.category().name()} != "asio.ssl") return false;
        BOOST_ASSERT(ec.message() == "short read");
        return true;
    }

    template <bool SSL>
    struct stream_kit {};

    template <>
    struct stream_kit<true> {
        boost::asio::ssl::context                          _ssl_context;
        boost::beast::ssl_stream<boost::beast::tcp_stream> _stream;

        template <typename Token>
        stream_kit(uri& uri, Token& token)
            : _ssl_context(boost::asio::ssl::context::tlsv12_client)
            , _stream(boost::asio::get_associated_executor(token), _ssl_context) {
            _ssl_context.add_certificate_authority(boost::asio::buffer(beastly::internal::mozilla_trust_chain.c_str(),
                                                                       beastly::internal::mozilla_trust_chain.size()));
            _ssl_context.set_verify_mode(boost::asio::ssl::verify_peer);

            if (!SSL_set_tlsext_host_name(_stream.native_handle(), uri.host().c_str())) {
                boost::beast::error_code ec{static_cast<int>(::ERR_get_error()),
                                            boost::asio::error::get_ssl_category()};
                throw boost::beast::system_error{ec};
            }
        }
    };

    template <>
    struct stream_kit<false> {
        boost::beast::tcp_stream _stream;

        template <typename Token>
        stream_kit(uri& uri, Token& token)
            : _stream(boost::asio::get_associated_executor(token)) {}
    };

    template <bool SSL, typename Token>
    class async_perform_request_impl {
    public:
        async_perform_request_impl(beastly::uri uri, boost::beast::http::request<boost::beast::http::string_body> req,
                                   Token& token)
            : _memory(std::make_unique<memory>(std::move(uri), std::move(req), token)) {}

        // Actions

        template <typename Self>
        void initiate_resolve(Self& self) {
            _memory->_state = memory::resolving;
            _memory->_resolver.async_resolve(
                _memory->_uri.host(), use_port_with_default(_memory->_uri), std::move(self));
        }

        template <typename Self>
        void initiate_connect(Self& self, const boost::asio::ip::tcp::resolver::results_type& resolver_results) {
            _memory->_state = memory::connecting;
            boost::beast::get_lowest_layer(_memory->_stream_kit._stream)
                .async_connect(resolver_results, std::move(self));
        }

        template <typename Self>
        void initiate_handshake(Self& self) {
            _memory->_state = memory::handshaking;
            _memory->_stream_kit._stream.async_handshake(boost::asio::ssl::stream_base::client, std::move(self));
        }

        template <typename Self>
        void initiate_write(Self& self) {
            _memory->_state = memory::writing;
            boost::beast::http::async_write(_memory->_stream_kit._stream, _memory->_request, std::move(self));
        }

        template <typename Self>
        void initiate_read(Self& self) {
            _memory->_state = memory::reading;
            boost::beast::http::async_read(
                _memory->_stream_kit._stream, _memory->_read_buffer, _memory->_response, std::move(self));
        }

        // Callbacks

        template <typename Self>
        void operator()(Self& self) {
            assert(_memory->_state == memory::starting);
            initiate_resolve(self);
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec,
                        const boost::asio::ip::tcp::resolver::results_type& resolver_results) {
            assert(_memory->_state == memory::resolving);
            if (ec)
                self.complete(ec, {});
            else {
                initiate_connect(self, resolver_results);
            }
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec,
                        const boost::asio::ip::tcp::resolver::results_type::endpoint_type&) {
            assert(_memory->_state == memory::connecting);
            if (ec)
                self.complete(ec, {});
            else {
                if constexpr (SSL) {
                    initiate_handshake(self);
                } else {
                    initiate_write(self);
                }
            }
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec) {
            switch (_memory->_state) {
            case memory::handshaking:
                if (ec)
                    self.complete(ec, {});
                else {
                    initiate_write(self);
                }
                break;
            default: assert(false); break;  // Should be unreachable.
            }
        }

        template <typename Self>
        void operator()(Self& self, boost::system::error_code ec, std::size_t bytes_transferred) {
            switch (_memory->_state) {
            case memory::writing:
                boost::ignore_unused(bytes_transferred);
                if (ec)
                    self.complete(ec, {});
                else {
                    initiate_read(self);
                }
                break;
            case memory::reading:
                if (ec && ec != boost::beast::errc::not_connected && ec != boost::asio::error::eof &&
                    !is_ssl_short_read(ec))
                    self.complete(ec, {});
                else {
                    if (_memory->_response.result() == boost::beast::http::status::moved_permanently) {
                        auto itr = _memory->_response.find(boost::beast::http::field::location);
                        if (itr == _memory->_response.end()) {
                            self.complete(boost::beast::make_error_code(boost::beast::error::timeout),
                                          {});  // TODO: Actually malformed response error. - got 301 moved_permanently
                                                // without a provided location
                        } else {
                            _memory->_uri = beastly::uri(itr->value());
                            _memory->_request.target(_memory->_uri.target());
                            _memory->_request.set(boost::beast::http::field::host, _memory->_uri.host());
                            initiate_resolve(self);
                        }
                    } else {
                        self.complete({}, std::move(_memory->_response));
                    }
                }
                break;
            default: assert(false); break;  // Should be unreachable.
            }
        }

    private:
        struct memory {
            enum { starting, resolving, connecting, handshaking, writing, reading } _state;
            uri                                                           _uri;
            boost::beast::http::request<boost::beast::http::string_body>  _request;
            boost::asio::ip::tcp::resolver                                _resolver;
            stream_kit<SSL>                                               _stream_kit;
            boost::beast::flat_buffer                                     _read_buffer;
            boost::beast::http::response<boost::beast::http::string_body> _response;

            memory(beastly::uri uri, boost::beast::http::request<boost::beast::http::string_body> req, Token& token)
                : _state(starting)
                , _uri(std::move(uri))
                , _request(std::move(req))
                , _resolver(boost::asio::get_associated_executor(token))
                , _stream_kit(_uri, token) {}
        };

        /**
         * Several components like Sockets really don't like to be moved, in spite
         * of technically supporting it.
         */
        std::unique_ptr<memory> _memory;
    };

}  // namespace beastly::internal
