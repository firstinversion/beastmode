#pragma once

#include <boost/asio/spawn.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <iostream>
#include <optional>

#include "boost_aliases.hpp"
#include "router.hpp"

namespace beastmode {

    class server {
    public:
        server() = default;

        server(const server&) = delete;
        server(server&&)      = delete;
        server& operator=(const server&) = delete;
        server& operator=(server&&) = delete;

        template <class Endpoint>
        void add_endpoint(const std::string& path);

        template <class Endpoint, class... PathComponents>
        void add_endpoint(const path<PathComponents...>& path);

        template <class Lambda>
        void add_get_handler(const std::string& path, Lambda&& lambda);

        template <class... PathComponents, class Lambda>
        void add_get_handler(const path<PathComponents...>& path, Lambda&& lambda);

        template <class Lambda>
        void add_post_handler(const std::string& path, Lambda&& lambda);

        template <class... PathComponents, class Lambda>
        void add_post_handler(const path<PathComponents...>& path, Lambda&& lambda);

        template <class Lambda>
        void add_put_handler(const std::string& path, Lambda&& lambda);

        template <class... PathComponents, class Lambda>
        void add_put_handler(const path<PathComponents...>& path, Lambda&& lambda);

        template <class Lambda>
        void add_delete_handler(const std::string& path, Lambda&& lambda);

        template <class... PathComponents, class Lambda>
        void add_delete_handler(const path<PathComponents...>& path, Lambda&& lambda);

        template <class Lambda>
        void add_options_handler(const std::string& path, Lambda&& lambda);

        template <class... PathComponents, class Lambda>
        void add_options_handler(const path<PathComponents...>& path, Lambda&& lambda);

        template <class ResourceT>
        void bind_resource(ResourceT& resource);

        void start(uint16_t port);
        void start(ip::tcp::endpoint endpoint);
        void join();
        void stop();

    private:
        router _router;

        struct executor {
            asio::io_context  _ioc;
            ip::tcp::endpoint _endpoint;
            ip::tcp::acceptor _acceptor;
            router&           _router;

            explicit executor(ip::tcp::endpoint endpoint, router& router);

            void do_accept();
        };

        std::optional<executor> _executor;
    };

    template <class Lambda>
    void server::add_get_handler(const std::string& path, Lambda&& lambda) {
        add_get_handler(make_path(path), std::forward<Lambda>(lambda));
    }

    template <class... PathComponents, class Lambda>
    void server::add_get_handler(const path<PathComponents...>& path, Lambda&& lambda) {
        _router.add_route(path, http::verb::get, std::forward<Lambda>(lambda));
    }

    template <class Lambda>
    void server::add_post_handler(const std::string& path, Lambda&& lambda) {
        add_post_handler(make_path(path), std::forward<Lambda>(lambda));
    }

    template <class... PathComponents, class Lambda>
    void server::add_post_handler(const path<PathComponents...>& path, Lambda&& lambda) {
        _router.add_route(path, http::verb::post, std::forward<Lambda>(lambda));
    }

    template <class Lambda>
    void server::add_put_handler(const std::string& path, Lambda&& lambda) {
        add_put_handler(make_path(path), std::forward<Lambda>(lambda));
    }

    template <class... PathComponents, class Lambda>
    void server::add_put_handler(const path<PathComponents...>& path, Lambda&& lambda) {
        _router.add_route(path, http::verb::put, std::forward<Lambda>(lambda));
    }

    template <class Lambda>
    void server::add_delete_handler(const std::string& path, Lambda&& lambda) {
        add_delete_handler(make_path(path), std::forward<Lambda>(lambda));
    }

    template <class... PathComponents, class Lambda>
    void server::add_delete_handler(const path<PathComponents...>& path, Lambda&& lambda) {
        _router.add_route(path, http::verb::delete_, std::forward<Lambda>(lambda));
    }

    template <class Lambda>
    void server::add_options_handler(const std::string& path, Lambda&& lambda) {
        add_options_handler(make_path(path), std::forward<Lambda>(lambda));
    }

    template <class... PathComponents, class Lambda>
    void server::add_options_handler(const path<PathComponents...>& path, Lambda&& lambda) {
        _router.add_route(path, http::verb::options, std::forward<Lambda>(lambda));
    }

    template <class Endpoint>
    void server::add_endpoint(const std::string& path) {
        add_endpoint<Endpoint>(make_path(path));
    }

    template <class Endpoint, class... PathComponents>
    void server::add_endpoint(const path<PathComponents...>& path) {
        _router.add_route<&Endpoint::get>(path, http::verb::get);
        _router.add_route<&Endpoint::post>(path, http::verb::post);
        _router.add_route<&Endpoint::put>(path, http::verb::put);
        _router.add_route<&Endpoint::delete_>(path, http::verb::delete_);
        _router.add_route<&Endpoint::options>(path, http::verb::options);
    }

    template <class ResourceT>
    void server::bind_resource(ResourceT& resource) {
        _router.bind_resource(resource);
    }

}  // namespace beastmode
