#pragma once

#include <boost/asio.hpp>
#include <boost/beast.hpp>

namespace beastmode {

    namespace asio  = boost::asio;
    namespace ip    = asio::ip;
    namespace beast = boost::beast;
    namespace http  = beast::http;

}  // namespace beastmode
