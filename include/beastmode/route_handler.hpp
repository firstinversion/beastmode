#pragma once

#include <boost/asio/spawn.hpp>
#include <type_traits>

#include "boost_aliases.hpp"
#include "capture_parameter.hpp"

namespace beastmode {

    template <class FuncPtr>
    struct route_handler;

    template <class Param>
    struct param_extractor;

    template <class Param>
    struct fix {
        static bool call_function(std::vector<std::string>&                      captures,
                                  std::unordered_map<std::type_index, resource>& resource_map,
                                  const http::request<http::string_body>& req, http::response<http::string_body>& res) {
            if constexpr (!std::is_base_of_v<resource_base, Param> && !std::is_same_v<asio::yield_context, Param> &&
                          param_extractor<Param>::CONSTRUCT_IN_RESOURCE_MAP) {
                return route_handler<decltype(&Param::operator())>::template call_function<&Param::operator()>(
                    captures, resource_map, req, res);
            }
            return true;
        }
    };

    template <class ReturnType, class Handler, class... Params>
    struct route_handler<ReturnType (Handler::*)(Params...)> {
        using FuncType = ReturnType(Params...);
        static_assert(std::is_function_v<FuncType>);
        static_assert(std::is_same_v<void, ReturnType> || std::is_same_v<bool, ReturnType>);

        static constexpr bool HAS_RETURN_VALUE = !std::is_same_v<void, ReturnType>;

        template <FuncType Handler::*FUNC>
        static bool call_function(std::vector<std::string>&                      captures,
                                  std::unordered_map<std::type_index, resource>& resource_map,
                                  const http::request<http::string_body>& req, http::response<http::string_body>& res) {
            if (resource_map.count(std::type_index(typeid(Handler))) == 0) {
                Handler& h = resource_map[std::type_index(typeid(Handler))].emplace<Handler>(req, res);
                bool     success =
                    (fix<std::decay_t<Params>>::call_function(captures, resource_map, req, res) && ... && true);
                if (success) {
                    if constexpr (HAS_RETURN_VALUE) {
                        success = (h.*FUNC)(param_extractor<std::decay_t<Params>>::get(captures, resource_map)...);
                    } else {
                        (h.*FUNC)(param_extractor<std::decay_t<Params>>::get(captures, resource_map)...);
                    }
                }
                return success;
            } else {
                return true;
            }
        }
    };

    template <class Lambda>
    struct lambda_wrapper {
        Lambda lambda;
    };

    template <class Param>
    struct param_extractor {
        static constexpr bool CONSTRUCT_IN_RESOURCE_MAP = true;

        static decltype(auto) get(const std::vector<std::string>&                captures,
                                  std::unordered_map<std::type_index, resource>& resource_map) {
            return resource_map[std::type_index(typeid(std::decay_t<Param>))].get<std::decay_t<Param>>();
        }
    };

    template <class Lambda>
    struct param_extractor<lambda_wrapper<Lambda>> {
        static constexpr bool CONSTRUCT_IN_RESOURCE_MAP = false;

        static decltype(auto) get(const std::vector<std::string>&                captures,
                                  std::unordered_map<std::type_index, resource>& resource_map) {
            return resource_map[std::type_index(typeid(lambda_wrapper<Lambda>))].get<lambda_wrapper<Lambda>>();
        }
    };

    template <size_t IDX, class Type>
    struct param_extractor<capture_parameter<IDX, Type>> {
        static constexpr bool CONSTRUCT_IN_RESOURCE_MAP = false;

        static capture_parameter<IDX, Type> get(std::vector<std::string>&                      captures,
                                                std::unordered_map<std::type_index, resource>& resource_map) {
            assert(!captures.empty());
            // TODO: Type Conversions
            static_assert(std::is_same_v<Type, std::string>,
                          "Only std::string is supported as capture parameter currently");
            // TODO: Check IDX
            return capture_parameter<IDX, Type>{captures[IDX]};
        }
    };

    template <>
    struct param_extractor<http::request<http::string_body>> {
        static constexpr bool CONSTRUCT_IN_RESOURCE_MAP = false;

        static const http::request<http::string_body>&
        get(std::vector<std::string>& captures, std::unordered_map<std::type_index, resource>& resource_map) {
            return resource_map[std::type_index(typeid(http::request<http::string_body>))]
                .get<http::request<http::string_body>>();
        }
    };

    template <>
    struct param_extractor<http::response<http::string_body>> {
        static constexpr bool CONSTRUCT_IN_RESOURCE_MAP = false;

        static http::response<http::string_body>& get(std::vector<std::string>&                      captures,
                                                      std::unordered_map<std::type_index, resource>& resource_map) {
            return resource_map[std::type_index(typeid(http::response<http::string_body>))]
                .get<http::response<http::string_body>>();
        }
    };

}  // namespace beastmode
