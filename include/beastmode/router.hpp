#pragma once

#include <functional>
#include <unordered_map>

#include <boost/beast.hpp>

#include "boost_aliases.hpp"
#include "endpoint.hpp"
#include "resource.hpp"
#include "route.hpp"
#include "route_handler.hpp"
#include "route_map.hpp"

namespace beastmode {

    class router {
    public:
        router();

        router(const router&) = delete;
        router(router&&)      = delete;

        template <auto HandlerFuncPointer, class... PathComponents>
        void add_route(const path<PathComponents...>& p, http::verb verb);

        template <class... PathComponents, class Lambda>
        void add_route(const path<PathComponents...>& p, http::verb verb, Lambda&& lambda);

        template <class ResourceT>
        void bind_resource(ResourceT& resource);

        template <class ResourceT>
        void unbind_resource();

        template <class ResourceT>
        void unbind_resource(ResourceT& resource);

        void handle(const route& route, const http::request<http::string_body>& req,
                    http::response<http::string_body>& res);

    private:
        using endpoint_func =
            std::function<void(std::vector<std::string>&, std::unordered_map<std::type_index, resource>&,
                               const http::request<http::string_body>& req, http::response<http::string_body>& res)>;
        class data {
        public:
            data() = default;
            endpoint_func&       operator[](http::verb verb) { return _array[static_cast<size_t>(verb)]; }
            const endpoint_func& operator[](http::verb verb) const { return _array[static_cast<size_t>(verb)]; }

        private:
            std::array<endpoint_func, 9> _array;
        };
        using route_map = beastmode::route_map<data>;
        route_map                                     _route_map;
        std::unordered_map<std::type_index, resource> _resource_map;
    };

    template <class FuncPtr>
    struct lambda_as_endpoint;

    template <class ReturnType, class LambdaClass, class... Params>
    struct lambda_as_endpoint<ReturnType (LambdaClass::*)(Params...)> : endpoint {
        using endpoint::endpoint;

        ReturnType operator()(lambda_wrapper<LambdaClass>& lambda_instance, Params... params) {
            return lambda_instance.lambda(std::forward<Params>(params)...);
        }
    };

    template <class ReturnType, class LambdaClass, class... Params>
    struct lambda_as_endpoint<ReturnType (LambdaClass::*)(Params...) const> : endpoint {
        using endpoint::endpoint;

        ReturnType operator()(lambda_wrapper<LambdaClass>& lambda_instance, Params... params) {
            return lambda_instance.lambda(std::forward<Params>(params)...);
        }
    };

    template <class... PathComponents, class Lambda>
    void router::add_route(const path<PathComponents...>& p, http::verb verb, Lambda&& lambda) {
        auto result = _route_map.emplace(p);
        // TODO: What if insert failed (result.second == false)?
        if (result.first) {
            data& d = *result.first;
            d[verb] = [lambda_instance = lambda_wrapper<std::decay_t<Lambda>>{std::forward<Lambda>(lambda)}](
                          std::vector<std::string>&                      captures,
                          std::unordered_map<std::type_index, resource>& resource_map,
                          const http::request<http::string_body>&        req,
                          http::response<http::string_body>&             res) mutable {
                // Add Resource
                resource_map.try_emplace(std::type_index(typeid(lambda_wrapper<std::decay_t<Lambda>>)),
                                         &lambda_instance);

                using Endpoint = lambda_as_endpoint<decltype(&Lambda::operator())>;
                constexpr auto func =
                    &route_handler<decltype(&Endpoint::operator())>::template call_function<&Endpoint::operator()>;
                func(captures, resource_map, req, res);
            };
        } else {
            // TODO: Failed to insert or find data?
        }
    }

    template <auto HandlerFuncPointer, class... PathComponents>
    void router::add_route(const path<PathComponents...>& p, http::verb verb) {
        auto result = _route_map.emplace(p);
        // TODO: What if insert failed (result.second == false)?
        if (result.first) {
            data& d = *result.first;
            d[verb] = [](std::vector<std::string>&                      captures,
                         std::unordered_map<std::type_index, resource>& resource_map,
                         const http::request<http::string_body>&        req,
                         http::response<http::string_body>&             res) {
                route_handler<decltype(HandlerFuncPointer)>::template call_function<HandlerFuncPointer>(
                    captures, resource_map, req, res);
            };
        } else {
            // TODO: Failed to insert or find data?
        }
    }

    template <class ResourceT>
    void router::bind_resource(ResourceT& resource) {
        _resource_map.try_emplace(std::type_index(typeid(ResourceT)), &resource);
    }

    template <class ResourceT>
    void router::unbind_resource() {
        _resource_map.erase(std::type_index(typeid(ResourceT)));
    }

    template <class ResourceT>
    void router::unbind_resource(ResourceT& resource) {
        unbind_resource<ResourceT>();
    }

}  // namespace beastmode
