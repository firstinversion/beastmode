#pragma

#include <iostream>
#include <memory>

#include "boost_aliases.hpp"
#include "router.hpp"

namespace beastmode {

    class session {
    public:
        explicit session(router& router, ip::tcp::socket&& socket);

        void start(asio::yield_context yield);

    private:
        void run(asio::yield_context yield);

        router&                           _router;  // TODO: Can we make this const?
        ip::tcp::socket                   _socket;
        beast::flat_buffer                _buffer;
        http::request<http::string_body>  _request;
        http::response<http::string_body> _response;
    };

}  // namespace beastmode
