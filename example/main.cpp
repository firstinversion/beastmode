#include <beastmode/endpoint.hpp>
#include <beastmode/server.hpp>
#include <beastmode/service.hpp>
#include <beastmode/query_parameter.hpp>
#include <csignal>

class EndpointStaticValue : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get() { response().body() = static_value; }

    void put() { static_value = request().body(); }

private:
    static inline std::string static_value = "";
};

struct StringResource : beastmode::resource_base {
    std::string value = "";
};

class EndpointResourceValue : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(StringResource& resource) {
        std::cout << &resource << std::endl;
        response().body() = resource.value;
        std::cout << "NEXT TEST2" << std::endl;
    }

    void put(StringResource& resource) { resource.value = request().body(); }
};

class ServiceTrivial : beastmode::service {
public:
    using service::service;

    void operator()() { value = request().body(); }

    const std::string& getValue() { return value; }

private:
    std::string value;
};

class EndpointWithServiceTrivial : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void put(ServiceTrivial& svc) { response().body() = svc.getValue(); }
};

class ServiceComplex : beastmode::service {
public:
    using service::service;

    void operator()(StringResource& resource, ServiceTrivial& other_svc) {
        resource.value = "resource: " + other_svc.getValue();
        size           = resource.value.size();
    }

    size_t size = 0;
};

class EndpointWithServiceComplex : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void put(ServiceComplex& svc, StringResource& resource) {
        response().set(boost::beast::http::field::cost, svc.size);
        response().body() = resource.value;
    }
};

class ServiceFail : public beastmode::service {
public:
    using service::service;

    bool operator()(ServiceFail& svc, StringResource& resource) {
        if (request().body() == "success") {
            return true;
        } else {
            response().base().result(boost::beast::http::status::internal_server_error);
            response().body() = "Body must be set to 'success'.";
            return false;
        }
    }
};

class ServiceFailTest : public beastmode::service {
public:
    using service::service;

    void operator()(ServiceComplex& svc, StringResource& resource) { std::cout << "MiddleWareFailTest" << std::endl; }
};

class EndpointWithServiceFail : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void put(const ServiceFail&, const ServiceFailTest&) { std::cout << "HandlerMWFail" << std::endl; }
};

class EndpointYield : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(boost::asio::yield_context yield) { std::cout << "I have a yield_context" << std::endl; }
};

class EndpointPost : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void post() { std::cout << "Post Body: " << request().body() << std::endl; }
};

class EndpointCapture1 : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(beastmode::capture_parameter<0> capture1) { std::cout << "Capture 1: " << *capture1 << std::endl; }
};

class EndpointCapture2 : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(beastmode::capture_parameter<0> capture1, beastmode::capture_parameter<1> capture2) {
        std::cout << "Capture 1: " << *capture1 << std::endl;
        std::cout << "Capture 2: " << *capture2 << std::endl;
    }
};

class EndpointCapture3 : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(beastmode::capture_parameter<2> capture3, beastmode::capture_parameter<1> capture2,
             beastmode::capture_parameter<0> capture1) {
        std::cout << "Capture 1: " << *capture1 << std::endl;
        std::cout << "Capture 2: " << *capture2 << std::endl;
        std::cout << "Capture 3: " << *capture3 << std::endl;
    }
};

class EndpointCapture4 : public beastmode::endpoint {
public:
    using endpoint::endpoint;

    void get(beastmode::capture_parameter<0> capture1, beastmode::query_parameters query_params) {
        std::cout << "Capture 1: " << *capture1 << std::endl;

        for(auto&&[key, value] : query_params.params()) {
            std::cout << "Query Param: " << key << " = " << value << std::endl;
        }
    }
};

std::function<void(int)> shutdown_handler;
void                     signal_handler(int signal) { shutdown_handler(signal); }

int main() {
    StringResource string_resource;

    beastmode::server server{};
    server.bind_resource(string_resource);
    server.add_endpoint<EndpointStaticValue>("/static_value");
    server.add_endpoint<EndpointResourceValue>("/resource_value");
    server.add_endpoint<EndpointWithServiceTrivial>("/svc_trivial");
    server.add_endpoint<EndpointWithServiceComplex>("/svc_complex");
    server.add_endpoint<EndpointWithServiceFail>("/svc_fail");
    server.add_endpoint<EndpointYield>("/yield");
    server.add_endpoint<EndpointPost>("/post_test");
    server.add_endpoint<EndpointCapture1>("/capture_test/" + beastmode::capture() + "/");
    server.add_endpoint<EndpointCapture2>("/capture_test/" + beastmode::capture() + "/" + beastmode::capture() + "/");
    server.add_endpoint<EndpointCapture3>("/capture_test/" + beastmode::capture() + "/" + beastmode::capture() + "/" +
                                          beastmode::capture() + "/");
    server.add_endpoint<EndpointCapture4>("/multi_capture/" + beastmode::capture() + "/");

    server.add_get_handler("/lambda_test", []() { std::cout << "Lambda!" << std::endl; });

    server.add_get_handler("/lambda_test2",
                           [](const beastmode::http::request<beastmode::http::string_body>& req,
                              beastmode::http::response<beastmode::http::string_body>&      res) {
                               std::cout << req.body() << std::endl;
                               res.body() = "test";
                           });

    server.add_get_handler("/lambda_test_mutable", []() mutable { std::cout << "Mutable Lambda!" << std::endl; });
    server.add_get_handler("/lambda_test_capture/" + beastmode::capture() + "/",
                           [](beastmode::capture_parameter<0> capture) mutable {
                               std::cout << "Lambda with capture: " << *capture << std::endl;
                           });

    server.add_get_handler("/lambda_test_w_service", [](ServiceTrivial svc) mutable {
        std::cout << "Lambda with service: " << svc.getValue() << std::endl;
    });

    server.add_post_handler("/lambda_test_w_service", [](ServiceTrivial svc) mutable {
        std::cout << "Lambda with service: " << svc.getValue() << std::endl;
    });

    server.add_options_handler("/options", []() mutable { std::cout << "Options call\n"; });

    server.start(8080);

    shutdown_handler = [&](int signal) { server.stop(); };
    std::signal(SIGTERM, &signal_handler);
    server.join();

    return 0;
}
