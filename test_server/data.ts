import * as faker from 'faker';

faker.seed(2947426);

export interface User {
    id: number
    first_name: string,
    last_name: string
}

export const users: User[] = buildUsers();

function buildUsers(): User[] {
    let users: User[] = [];

    for (let i = 0; i < 12; i++) {
        users.push({
            id: i + 1,
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
        });
    }

    return users;
}
