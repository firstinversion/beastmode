import * as express from 'express';
import {users} from './data';

const port = 3000;
const server = express();

server.get('/message', (req: express.Request, res: express.Response) => {
    res.json({message: 'Hello World!'});
});

server.get('/api/users', (req: express.Request, res: express.Response) => {
    res.send(users);
});

server.get('/api/users/:id', (req: express.Request, res: express.Response) => {
    const id = parseInt(req.params.id);
    if (typeof id == 'number') {
        if (isNaN(id)) {
            res.sendStatus(404);
        } else if (id > 0 && id <= users.length) {
            res.send(users[req.params.id - 1]);
        } else {
            res.sendStatus(404);
        }
    } else {
        res.sendStatus(404);
    }
});

server.use((req, res, next) => {
    console.log(req);
    console.log(res);
    next();
});
server.listen(port, (err: any) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
});
